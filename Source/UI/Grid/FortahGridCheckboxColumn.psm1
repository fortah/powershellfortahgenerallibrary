using module '.\FortahGridColumn.psm1'
using module '..\..\General\FortahCharacter.psm1'

class FortahGridCheckboxColumn : FortahGridColumn {
    
    FortahGridCheckboxColumn([string] $pHeader, [int] $pWidth) : base($pHeader, $pWidth) {
    }

    [Object] transformValue([Object] $pValue) {
        if ($pValue) {
            return [FortahCharacter]::on
        } else {
            return [FortahCharacter]::off
        }
    }
            
}