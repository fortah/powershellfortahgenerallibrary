using namespace System.Collections.Generic

class FortahPropertyGridGeometry {

    hidden [int] $nameWidth = 0
    [int] getNameWidth() { return $this.nameWidth }
    [void] setNameWidth([int] $pValue) { $this.nameWidth = $pValue }

    hidden [int] $valueWidth = 0
    [int] getValueWidth() { return $this.valueWidth }
    [void] setValueWidth([int] $pValue) { $this.valueWidth = $pValue }

    FortahPropertyGridGeometry() {
    }

    FortahPropertyGridGeometry([int] $pNameWidth) {
        $this.nameWidth = $pNameWidth
    }

    FortahPropertyGridGeometry([int] $pNameWidth, [int] $pValueWidth) {
        $this.nameWidth = $pNameWidth
        $this.valueWidth = $pValueWidth
    }

    static [List[int]] parse([List[int]] $pWidths) {
        [List[FortahPropertyGridGeometry]] $geometries = [List[FortahPropertyGridGeometry]]::new()
        [FortahPropertyGridGeometry] $geometry = $null
        for ([int] $index = 0; $index -lt $pWidths.Count; $index++) {
            if ($index % 2 -eq 0) {
                $geometry = [FortahPropertyGridGeometry]::new($pWidths[$index])
            } else {
                $geometry.setValueWidth($pWidths[$index])
                $geometries.Add($geometry)
            }
        }
        return $geometries
    }
    
}