using module '.\FortahGrid.psm1'
using module '.\FortahGridCheckboxColumn.psm1'
using module '.\FortahGridColours.psm1'
using module '.\FortahGridColumn.psm1'
using module '.\FortahGridColumnColours.psm1'
using module '.\FortahSelectableGridItem.psm1'
using module '..\General\FortahColour.psm1'
using module '..\General\FortahColours.psm1'
using module '..\General\FortahPowerShell.psm1'
using module '..\General\FortahStateColours.psm1'
using module '..\..\Runtime\FortahCommand.psm1'

using namespace System.Collections.Generic

class FortahSelectableGrid : FortahGrid {
    
    hidden [List[Object]] $items = [List[Object]]::new()
    [List[Object]] getItems() { return $this.items }
    [void] setItems([List[Object]] $pValue) { $this.items = $pValue }

    hidden [int] $commandIndex = 0
    [int] getCommandIndex() { return $this.commandIndex }
    [void] setCommandIndex([int] $pValue) { $this.commandIndex = $pValue }

    FortahSelectableGrid([int] $pWidth) : base($pWidth) {
        $this.FortahSelectableGridEx()
    }
    
    FortahSelectableGrid([string] $pHeader, [int] $pWidth) : base($pHeader, $pWidth) {
        $this.FortahSelectableGridEx()
    }

    FortahSelectableGrid([string] $pHeader, [int] $pWidth, [List[Object]] $pColumns) : base($pHeader, $pWidth) {
        $this.FortahSelectableGridEx()
        $this.addColumns($pColumns)
    }

    hidden [void] FortahSelectableGridEx() {
        $this.addColumns([List[FortahGridColumn]]::new(@(
            [FortahGridCheckboxColumn]::new('Sel', 3),
            [FortahGridColumn]::new('Com', 3, [FortahSelectableGrid]::createCommandColours())
        )))
    }

    [void] renderRow([Object] $pData) {
        $this.commandIndex++
        [string] $commandKey = $this.commandIndex.ToString()
        [FortahCommand] $command = [FortahCommand]::new('Select' + $commandKey, $commandKey, $commandKey)
        [FortahSelectableGridItem] $item = [FortahSelectableGridItem]::new($command, $pData)
        $this.items.Add($item)
        [List[object]] $dataToRender = $null
        if ($pData -is [List[object]]) {
            $dataToRender = $pData
        } else {
            $dataToRender = [List[object]]::new(@(pData))
        }
        $dataToRender.Insert(0, $item.isSelected())
        $dataToRender.Insert(1, $item.getCommand().getKey())
        ([FortahGrid]$this).renderRow($dataToRender)
    }
}