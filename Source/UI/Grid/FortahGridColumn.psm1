using module '..\Controls\FortahLabel.psm1'
using module '..\..\General\FortahColour.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\General\FortahPowerShell.psm1'
using module '..\..\General\FortahState.psm1'

using namespace System.Collections
using namespace System.Management.Automation

class FortahGridColumn {

    hidden [string] $header = ''
    [string] getHeader() { return $this.header }
    [void] setHeader([string] $pValue) { $this.header = $pValue }
    
    hidden [int] $width = 0
    [int] getWidth() { return $this.width }
    [void] setWidth([int] $pValue) { $this.width = $pValue }

    hidden [FortahColour] $colour = [FortahColour]::null
    [FortahColour] getColour() { return $this.colour }
    [void] setColour([FortahColour] $pValue) { $this.colour = $pValue }

    hidden [ScriptBlock] $binding = $null
    [ScriptBlock] getBinding() { return $this.binding }
    [void] setBinding([ScriptBlock] $pValue) { $this.binding = $pValue }

    FortahGridColumn([string] $pHeader, [int] $pWidth) {
        $this.header = $pHeader
        $this.width = $pWidth
    }

    FortahGridColumn([string] $pHeader, [int] $pWidth, [FortahColour] $pColour) {
        $this.header = $pHeader
        $this.width = $pWidth
        $this.colour = $pColour
    }

    FortahGridColumn([string] $pHeader, [int] $pWidth, [ScriptBlock] $pBinding) {
        $this.header = $pHeader
        $this.width = $pWidth
        $this.binding = $pBinding
    }

    FortahGridColumn([string] $pHeader, [int] $pWidth, [FortahColour] $pColour, [ScriptBlock] $pBinding) {
        $this.header = $pHeader
        $this.width = $pWidth
        $this.colour = $pColour
        $this.binding = $pBinding
    }

    [Object] getValue([Object] $pData, [int] $pIndex) {
        [Object] $value = $null
        if ($null -ne $this.binding) {
            $value = $this.binding.Invoke($pData)
        } else {
            $value = $pData[$pIndex]
        }
        return $this.transformValue($value)
    }

    [Object] transformValue([Object] $pValue) {
        return $pValue
    }

    [FortahLabel] toHeaderLabel() {
        return $this.toLabel($this.header, [FortahColours]::headerColour)
    }

    [FortahLabel] toLabel([Object] $pValue) {
        return $this.toLabel($pValue, [FortahState]::normal)
    }
    
    [FortahLabel] toLabel([Object] $pValue, [FortahState] $pState) {
        [FortahColour] $labelColour = $this.colour
        if ($labelColour -eq [FortahColour]::null) {
            $labelColour = [FortahColours]::getStateColour($pState)
        }
        return $this.toLabel($pValue, $labelColour)
    }

    [FortahLabel] toLabel([Object] $pValue, [FortahColour] $pColour) {
        return [FortahLabel]::new($pValue, $this.width, $pColour)
    }       
}