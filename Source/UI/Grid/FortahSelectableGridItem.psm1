using module '..\Application\FortahCommand.psm1'

using namespace System

class FortahSelectableGridItem {

    hidden [FortahCommand] $command = [FortahCommand]::new()
    [FortahCommand] getCommand() { return $this.command }
    [void] setCommand([FortahCommand] $pValue) { $this.command = $pValue }

    hidden [Object] $data = [Object]::new()
    [Object] getData() { return $this.data }
    [void] setData([Object] $pValue) { $this.data = $pValue }

    hidden [bool] $selected = $false
    [bool] isSelected() { return $this.selected }
    [void] setSelected([bool] $pValue) { $this.selected = $pValue }

    FortahSelectableGridItem([FortahCommand] $pCommand, [Object] $pData) {
        $this.setCommand($pCommand)
        $this.setData($pData)
    }

    FortahSelectableGridItem([FortahCommand] $pCommand, [Object] $pData, [bool] $pSelected) {
        $this.setCommand($pCommand)
        $this.setData($pData)
        $this.setSelected($pSelected)
    }    

}