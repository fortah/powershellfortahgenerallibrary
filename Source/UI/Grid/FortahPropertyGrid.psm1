using module '..\..\General\FortahColours.psm1'
using module '.\FortahGrid.psm1'
using module '.\FortahGridColumn.psm1'
using module '.\FortahPropertyGridGeometry.psm1'

using namespace System.Collections.Generic

class FortahPropertyGrid : FortahGrid {

    FortahPropertyGrid([List[int]] $pColumnWidths) : base() {
        $this.createColumns($pColumnWidths)

    }

    FortahPropertyGrid([string] $pHeader, [List[int]] $pColumnWidths) : base($pHeader) {
        $this.createColumns($pColumnWidths)
    }

    FortahPropertyGrid([string] $pName, [string] $pHeader, [List[int]] $pColumnWidths) : base($pName, $pHeader) {
        $this.createColumns($pColumnWidths)
    }

    hidden [void] createColumns([List[int]] $pColumnWidths) {
        [List[FortahPropertyGridGeometry]] $geometries = [FortahPropertyGridGeometry]::parse($pColumnWidths)
        [FortahPropertyGridGeometry] $geometry = $null
        foreach ($geometry in $geometries) {
            $this.columns.Add([FortahGridColumn]::new('', $geometry.getNameWidth(), [FortahColours]::textColour))
            $this.columns.Add([FortahGridColumn]::new('', $geometry.getValueWidth(), [FortahColours]::selectedColour))
        }
    }
    
}