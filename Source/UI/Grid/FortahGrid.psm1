using module '.\FortahGridColumn.psm1'
using module '..\Controls\FortahControl.psm1'
using module '..\Controls\FortahFrame.psm1'
using module '..\Controls\FortahLabel.psm1'
using module '..\Controls\FortahSeparator.psm1'
using module '..\Controls\FortahShelf.psm1'
using module '..\Controls\FortahStack.psm1'
using module '..\..\General\FortahColour.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\General\FortahState.psm1'

using namespace System
using namespace System.Collections.Generic
using namespace System.Management.Automation

class FortahGrid : FortahFrame {

    hidden [string] $header = ''
    [string] getHeader() { return $this.header }
    [void] setHeader([string] $pValue) { $this.header = $pValue }

    hidden [List[FortahGridColumn]] $columns = [List[FortahGridColumn]]::new()
    [List[FortahGridColumn]] getColumns() { return $this.columns }
    [void] setColumns([List[FortahGridColumn]] $pValue) { $this.columns = $pValue }

    hidden [ScriptBlock] $stateBinding = $null
    [ScriptBlock] getStateBinding() { return $this.stateBinding }
    [void] setStateBinding([ScriptBlock] $pValue) { $this.stateBinding = $pValue }

    hidden [Object] $data = [Object]::new()
    [Object] getData() { return $this.data }
    [void] setData([Object] $pValue) { $this.data = $pValue }

    FortahGrid() : base() {        
    }

    FortahGrid([string] $pName) : base($pName) {
    }

    FortahGrid([List[FortahGridColumn]] $pColumns) : base() {
        $this.columns = $pColumns
    }

    FortahGrid([int] $pWidth, [List[FortahGridColumn]] $pColumns) : base() {
        $this.width = $pWidth
        $this.columns = $pColumns
    }

    FortahGrid([string] $pName, [int] $pWidth, [List[FortahGridColumn]] $pColumns) : base($pName) {
        $this.width = $pWidth
        $this.columns = $pColumns
    }

    FortahGrid([string] $pHeader, [List[FortahGridColumn]] $pColumns) : base() {
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    FortahGrid([string] $pName, [string] $pHeader, [List[FortahGridColumn]] $pColumns) : base($pName) {
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    FortahGrid([int] $pWidth, [string] $pHeader, [List[FortahGridColumn]] $pColumns) : base() {
        $this.width = $pWidth
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    FortahGrid([string] $pName, [int] $pWidth, [string] $pHeader, [List[FortahGridColumn]] $pColumns) : base($pName) {
        $this.width = $pWidth
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    [List[FortahControl]] toRenderables() {
        $this.updateWidth()
        $this.controls = [List[FortahControl]]::new()
        $this.addHeaderControls()
        $this.addColumnHeadersControls()
        $this.addDataControls()
        return ([FortahFrame]$this).toRenderables()
    }

    hidden [void] addHeaderControls() {
        if ($this.header.Length -gt 0) {
            $this.controls.Add([FortahLabel]::new($this.header), [FortahColours]::headerColour)
            $this.controls.Add([FortahSeparator]::new())
        }
    }

    hidden [void] addColumnHeadersControls() {
        if ($null -ne ($this.columns | Where-Object {$_.getHeader().Length -gt 0})) {
            [FortahShelf] $shelf = [FortahShelf]::new()
            [FortahGridColumn] $column = $null
            foreach ($column in $this.columns) {
                $shelf.addControl($column.toHeaderLabel())
            }
            $this.controls.Add($shelf)
            $this.controls.Add([FortahSeparator]::new())
        }        
    }

    hidden [void] addDataControls() {
        [FortahStack] $stack = [FortahStack]::new()
        [List[Object]] $dataRow = $null
        foreach ($dataRow in $this.data) {
            [FortahShelf] $shelf = [FortahShelf]::new()
            [FortahState] $state = $this.getRowState($dataRow)
            [int] $index = 0
            [FortahGridColumn] $column = $null
            foreach ($column in $this.columns) {
                [Object] $value = $column.getValue($dataRow, $index)
                $shelf.addControl($column.toLabel($value, $state))
                $index++
            }
            $stack.addControl($shelf)
        }
        $this.controls.Add($stack)
    }

    hidden [FortahState] getRowState([List[Object]] $pDataRow) {
        [FortahState] $state = [FortahState]::normal
        if ($null -ne $this.stateBinding) {
            $state = $this.stateBinding.Invoke($pDataRow)
        }
        return $state
    }

    hidden [void] updateWidth() {
        [int] $titleWidth = 0
        if ($this.title -ne '') {
            $titleWidth = $this.title.Length + 4
            if ($titleWidth -gt $this.width) {
                $this.width = $titleWidth
            }
        }
        [int] $columnsWidth = 2
        [FortahGridColumn] $column = $null
        foreach ($column in $this.columns) {
            $columnsWidth += $column.width + 2
        }
        if ($columnsWidth -gt $this.width) {
            $this.width = $columnsWidth
        }
    }     
     
}
