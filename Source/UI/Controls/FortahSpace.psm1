using module '.\FortahControl.psm1'
using module '..\..\General\FortahConsole.psm1'

class FortahSpace : FortahControl {

    FortahSpace() : base() { 
    }

    FortahSpace([string] $pName) : base($pName) { 
    }

    FortahSpace([int] $pWidth) : base($pWidth) { 
    }

    FortahSpace([string] $pName, [int] $pWidth) : base($pName, $pWidth) { 
    }

    [void] render() {
        if ($this.width -gt 0) {
            [FortahConsole]::write(' ' * $this.width)
        }
    }

}