using module '.\FortahControl.psm1'
using module '..\..\General\FortahColour.psm1'
using module '..\..\Runtime\FortahCommand.psm1'

using namespace System.Collections.Generic

class FortahContainer : FortahControl {

    hidden [List[FortahControl]] $controls = [List[FortahControl]]::new()
    [List[FortahControl]] getControls() { return $this.controls }
    [void] setControls([List[FortahControl]] $pValue) { $this.controls = $pValue }

    FortahContainer() : base() {        
    }

    FortahContainer([string] $pName) : base($pName) {
    }

    FortahContainer([List[FortahControl]] $pControls) : base() {
        $this.controls = $pControls
    }
    
    FortahContainer([string] $pName, [List[FortahControl]] $pControls) : base($pName) {
        $this.controls = $pControls
    }

    FortahContainer([int] $pWidth, [List[FortahControl]] $pControls) : base($pWidth) {
        $this.controls = $pControls
    }

    FortahContainer([string] $pName, [int] $pWidth, [List[FortahControl]] $pControls) : base($pName, $pWidth) {
        $this.controls = $pControls
    }

    FortahContainer([FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pColour) {
        $this.controls = $pControls
    }

    FortahContainer([string] $pName, [FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pName, $pColour) {
        $this.controls = $pControls
    }

    FortahContainer([int] $pWidth, [FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pWidth, $pColour) {
        $this.controls = $pControls
    }

    FortahContainer([string] $pName, [int] $pWidth, [FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pName, $pWidth, $pColour) {
        $this.controls = $pControls
    }

    [void] addControl([FortahControl] $pControl) {
        $this.controls.Add($pControl)
    }

    [void] addControls([List[FortahControl]] $pControls) {
        $this.controls.AddRange($pControls)
    }

    [List[FortahControl]] toRenderables() {
        [List[FortahControl]] $renderables = [List[FortahControl]]::new()
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ( -not ($control.isHidden())) {
                [List[FortahControl]] $controlRenderables = $control.toRenderables()
                if ($null -ne $controlRenderables) {
                    $renderables.AddRange($controlRenderables)
                }
            }
        }
        return $renderables
    }

    [FortahControl] findControl([string] $pName) {
        [FortahControl] $controlFound = $null
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            $controlFound = $control.findControl($pName)
            if ($null -ne $controlFound) {
                break
            }
        }
        return $controlFound
    }

    [FortahCommand] findCommand([string] $pKey) {
        [FortahCommand] $command = $null
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            $command = $control.findCommand($pKey)
            if ($null -ne $command) {
                break
            }
        }
        return $command
    }
    
}