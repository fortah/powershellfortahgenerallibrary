using module '.\FortahControl.psm1'
using module '..\..\General\FortahConsole.psm1'

using namespace System.Collections.Generic

class FortahNewLine : FortahControl {

    hidden [int] $noOf = 1
    [int] getNoOf() { return $this.noOf }
    [void] setNoOf([int] $pValue) { $this.noOf = $pValue }

    FortahNewLine() : base() {        
    }
    
    FortahNewLine([string] $pName) : base($pName) {
    }

    FortahNewLine([int] $pNoOf) : base() {
        $this.noOf = $pNoOf        
    }

    FortahNewLine([string] $pName, [int] $pNoOf) : base($pName) {
        $this.noOf = $pNoOf        
    }

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }

    [void] render() {
        for ([int] $index = 0; $index -lt $this.noOf; $index++) {
            [FortahConsole]::newLine()
        }
    }
    
}