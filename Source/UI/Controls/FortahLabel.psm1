using module '.\FortahControl.psm1'
using module '..\..\General\FortahColour.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\General\FortahConsole.psm1'

using namespace System.Collections.Generic

class FortahLabel : FortahControl {

    hidden [string] $text = ''
    [string] getText() { return $this.text }
    [void] setText([string] $pValue) { $this.text = $pValue }
    
    FortahLabel([string] $pText) : base() {
        $this.text = $pText
    }

    FortahLabel([string] $pName, [string] $pText) : base($pName) {
        $this.text = $pText
    }

    FortahLabel([string] $pText, [int] $pWidth) : base($pWidth) {
        $this.text = $pText
    }

    FortahLabel([string] $pName, [string] $pText, [int] $pWidth) : base($pName, $pWidth) {
        $this.text = $pText
    }

    FortahLabel([string] $pText, [FortahColour] $pColour) : base($pColour) {
        $this.text = $pText
    }
    
    FortahLabel([string] $pName, [string] $pText, [FortahColour] $pColour) : base($pName, $pColour) {
        $this.text = $pText
    }

    FortahLabel([string] $pText, [int] $pWidth, [FortahColour] $pColour) : base($pWidth, $pColour) {
        $this.text = $pText
    }

    FortahLabel([string] $pName, [string] $pText, [int] $pWidth, [FortahColour] $pColour) : base($pName, $pWidth, $pColour) {
        $this.text = $pText
    }

    [int] calculateWidth() {
        [int] $width = $this.width
        if ($this.text.Length -gt $width) {
            $width = $this.text.Length
        }
        return $width
    }

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }

    [void] render() {
        [string] $textToRender = $this.text
        if ($this.width -gt 0) {
            if ($this.width -gt $textToRender.Length) {
                $textToRender += ' ' * ($this.width - $textToRender.Length)
            } else {
                if ($this.width -lt $textToRender.Length) {
                    $textToRender = $textToRender.Substring(0, $this.width)
                }
            }
        }
        [FortahConsole]::write($textToRender, $this.colour)
    }
}