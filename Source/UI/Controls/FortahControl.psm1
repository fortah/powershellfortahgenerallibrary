using module '..\..\General\FortahColour.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\Runtime\FortahCommand.psm1'

using namespace System.Collections.Generic

class FortahControl {

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }

    hidden [int] $width = 0
    [int] getWidth() { return $this.width }
    [void] setWidth([int] $pValue) { $this.width = $pValue }

    hidden [FortahColour] $colour = [FortahColours]::textColour
    [FortahColour] getColour() { return $this.colour }
    [void] setColour([FortahColour] $pValue) { $this.colour = $pValue }

    hidden [bool] $hidden = $false
    [bool] isHidden() { return $this.hidden }
    [void] setHidden([bool] $pValue) { $this.hidden = $pValue }

    [string] getUserInput() { return $null }

    FortahControl() {
    }

    FortahControl([string] $pName) {
        $this.name = $pName
    }

    FortahControl([int] $pWidth) {
        $this.width = $pWidth
    }

    FortahControl([string] $pName, [int] $pWidth) {
        $this.name = $pName
        $this.width = $pWidth
    }
    
    FortahControl([FortahColour] $pColour) {
        $this.colour = $pColour
    }

    FortahControl([string] $pName, [FortahColour] $pColour) {
        $this.name = $pName
        $this.colour = $pColour
    }
    
    FortahControl([int] $pWidth, [FortahColour] $pColour) {
        $this.width = $pWidth
        $this.colour = $pColour
    }

    FortahControl([string] $pName, [int] $pWidth, [FortahColour] $pColour) {
        $this.name = $pName
        $this.width = $pWidth
        $this.colour = $pColour
    }

    [int] calculateWidth() {
        return $this.width
    }

    [List[FortahControl]] toRenderables() {
        return $null
    }

    [void] appendToRenderables([List[FortahControl]] $pRenderables) {
        [List[FortahControl]] $thisRenderables = $this.toRenderables()
        if ($null -ne $thisRenderables) {
            $pRenderables.AddRange($thisRenderables)
        }
    }

    [void] render() {        
    }

    [FortahControl] findControl([string] $pName) {
        if ($this.name -eq $pName) {
            return $this
        } else {
            return $null
        }
    }

    [FortahCommand] findCommand([string] $pKey) {
        return $null
    }    
    
}