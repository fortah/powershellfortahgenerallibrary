using module '.\FortahControl.psm1'
using module '.\FortahLabel.psm1'
using module '.\FortahNewLine.psm1'
using module '.\FortahSeparator.psm1'
using module '.\FortahStack.psm1'
using module '..\..\General\FortahCharacter.psm1'
using module '..\..\General\FortahColour.psm1'
using module '..\..\General\FortahColours.psm1'

using namespace System.Collections.Generic

class FortahFrame : FortahStack {

    hidden [List[FortahControl]] $renderables = $null
    hidden [int] $width = 0
    hidden [int] $outerWidth = 0
    hidden [int] $currentWidth = 0

    FortahFrame() : base() {        
    }

    FortahFrame([int] $pWidth) : base() {
        $this.width = $pWidth
    }

    FortahFrame([string] $pName, [int] $pWidth) : base($pName) {
        $this.width = $pWidth
    }

    FortahFrame([List[FortahControl]] $pControls) : base($pControls) {
        $this.colour = [FortahColours]::borderColour
    }

    FortahFrame([string] $pName, [List[FortahControl]] $pControls) : base($pName, $pControls) {
        $this.colour = [FortahColours]::borderColour
    }

    FortahFrame([int] $pWidth, [List[FortahControl]] $pControls) : base($pControls) {
        $this.width = $pWidth
        $this.colour = [FortahColours]::borderColour
    }

    FortahFrame([string] $pName, [int] $pWidth, [List[FortahControl]] $pControls) : base($pName, $pControls) {
        $this.width = $pWidth
        $this.colour = [FortahColours]::borderColour
    }

    [int] calculateWidth() {
        $this.width = ([FortahStack]$this).calculateWidth()
        $this.outerWidth = $this.width + 4
        $this.currentWidth = 0
        return $this.width
    }

    [List[FortahControl]] toRenderables() {
        $this.renderables = [List[FortahControl]]::new()
        $this.calculateWidth()
        $this.addHeader()
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            [List[FortahControl]] $controlRenderables = $control.toRenderables()
            [FortahControl] $controlRenderable = $null
            foreach ($controlRenderable in $controlRenderables) {
                if ($controlRenderable -is [FortahNewLine]) {
                    $this.processNewLine([FortahNewLine]$controlRenderable)
                } else {
                    if ($controlRenderable -is [FortahSeparator]) {
                        $this.processSeparator([FortahSeparator]$controlRenderable)
                    } else {
                        $this.processControl($controlRenderable)
                    }
                }
            }
        }
        $this.addFooter()
        return $this.renderables
    }

    hidden [void] addHeader() {
        $this.addTopBorder()
        $this.addNewLine()
        $this.addLeftBorder()
    }

    hidden [void] processNewLine([FortahNewLine] $pNewLine) {
        for ([int] $index = 0; $index -lt $pNewLine.getNoOf(); $index++) {
            $this.addFill()
            $this.addRightBorder()
            $this.addNewLine()
            $this.addLeftBorder()
        }
    }

    hidden [void] processSeparator([FortahSeparator] $pSeparator) {
        $this.addFill()
        $this.addRightBorder()
        $this.addNewLine()
        $this.addMiddleBorder()
        $this.addNewLine()
        $this.addLeftBorder()        
    }

    hidden [void] processControl([FortahControl] $pControl) {
        $this.renderables.Add($pControl)
        $this.currentWidth += $pControl.calculateWidth()
    }

    hidden [void] addFooter() {
        $this.addFill()
        $this.addRightBorder()
        $this.addNewLine()
        $this.addBottomBorder()
    }

    hidden [void] addNewLine() {
        $this.renderables.Add([FortahNewLine]::new())
        $this.currentWidth = 0
    }

    hidden [void] addTopBorder() {
        $this.addHorizontalBorder([FortahCharacter]::topLeft, [FortahCharacter]::topRight)
    }

    hidden [void] addMiddleBorder() {
        $this.addHorizontalBorder([FortahCharacter]::leftJoin, [FortahCharacter]::rightJoin)
    }

    hidden [void] addBottomBorder() {
        $this.addHorizontalBorder([FortahCharacter]::bottomLeft, [FortahCharacter]::bottomRight)
    }    

    hidden [void] addHorizontalBorder([char] $pLeftChar, [char] $pRightChar) {
        $this.renderables.Add([FortahLabel]::new($pLeftChar + ([FortahCharacter]::horizontal.ToString() * ($this.outerWidth - 2)) + $pRightChar, $this.colour))
    }

    hidden [void] addLeftBorder() {
        $this.renderables.Add([FortahLabel]::new([FortahCharacter]::vertical + ' ', $this.colour))
    }

    hidden [void] addRightBorder() {
        $this.renderables.Add([FortahLabel]::new(' ' + [FortahCharacter]::vertical, $this.colour))
    }

    hidden [void] addFill() {
        if ($this.currentWidth -lt $this.width) {
            $this.renderables.Add([FortahLabel]::new(' ' * ($this.width - $this.currentWidth)))
        }
    }    
    
}