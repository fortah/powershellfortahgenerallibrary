using module '.\FortahContainer.psm1'
using module '.\FortahControl.psm1'
using module '.\FortahLabel.psm1'
using module '.\FortahNewLine.psm1'
using module '.\FortahSeparator.psm1'
using module '..\..\General\FortahConsole.psm1'

using namespace System.Collections.Generic

class FortahShelf : FortahContainer {

    hidden [int] $space = 1
    [int] getSpace() { return $this.space }
    [void] setSpace([int] $pValue) { $this.space = $pValue }    

    FortahShelf() : base() {
    }
    
    FortahShelf([string] $pName) : base($pName) {
    }

    FortahShelf([int] $pSpace) : base() {
        $this.space = $pSpace
    }

    FortahShelf([string] $pName, [int] $pSpace) : base($pName) {
        $this.space = $pSpace
    }

    FortahShelf([List[FortahControl]] $pControls) : base($pControls) {
    }

    FortahShelf([List[FortahControl]] $pControls, [int] $pSpace) : base($pControls) {
        $this.space = $pSpace
    }

    [int] calculateWidth() {
        [int] $width = $this.width
        [int] $controlsWidth = 0
        [bool] $first = $true
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ($first) {
                $first = $false
                $controlsWidth += $control.calculateWidth()
            } else {
                if ($control -is [FortahSeparator]) {
                    if ($controlsWidth -gt $width) {
                        $width = $controlsWidth
                    }
                    $controlsWidth = 0
                } else {
                    if ($this.space -gt 0) {
                        $controlsWidth += $this.space
                    }
                    $controlsWidth += $control.calculateWidth()
                }
            }
        }
        if ($controlsWidth -gt $width) {
            $width = $controlsWidth
        }
        return $width
    }

    [List[FortahControl]] toRenderables() {
        [List[FortahControl]] $renderables = [List[FortahControl]]::new()
        [bool] $first = $true
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ($first) {
                $first = $false
                $control.appendToRenderables($renderables)
            } else {
                if ($control -is [FortahSeparator]) {
                    $first = $true
                    $renderables.Add([FortahNewLine]::new())
                } else {
                    if ($this.space -gt 0) {
                        $renderables.Add([FortahLabel]::new(' ', $this.space))
                    }
                    $control.appendToRenderables($renderables)
                }
            }
        }
        return $renderables
    }
    
}