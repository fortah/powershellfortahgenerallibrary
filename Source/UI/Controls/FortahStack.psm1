using module '.\FortahContainer.psm1'
using module '.\FortahControl.psm1'
using module '.\FortahNewLine.psm1'
using module '.\FortahSeparator.psm1'
using module '..\..\General\FortahConsole.psm1'

using namespace System.Collections.Generic

class FortahStack : FortahContainer {

    hidden [int] $space = 1
    [int] getSpace() { return $this.space }
    [void] setSpace([int] $pValue) { $this.space = $pValue }  

    FortahStack() : base() {
    }
    
    FortahStack([string] $pName) : base($pName) {
    }
    
    FortahStack([int] $pSpace) : base() {
        $this.space = $pSpace
    }

    FortahStack([string] $pName, [int] $pSpace) : base($pName) {
        $this.space = $pSpace
    }

    FortahStack([List[FortahControl]] $pControls) : base($pControls) {
    }
    
    FortahStack([string] $pName, [List[FortahControl]] $pControls) : base($pName, $pControls) {
    }

    FortahStack([List[FortahControl]] $pControls, [int] $pSpace) : base($pControls) {
        $this.space = $pSpace
    }

    FortahStack([string] $pName, [List[FortahControl]] $pControls, [int] $pSpace) : base($pName, $pControls) {
        $this.space = $pSpace
    }

    [int] calculateWidth() {
        [int] $width = $this.width
        [int] $controlsWidth = 0
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            [int] $controlWidth = $control.calculateWidth()
            if ($controlWidth -gt $controlsWidth) {
                $controlsWidth = $controlWidth
            }
        }
        if ($controlsWidth -gt $width) {
            $width = $controlsWidth
        }
        return $width
    }

    [List[FortahControl]] toRenderables() {
        [List[FortahControl]] $renderables = [List[FortahControl]]::new()
        [bool] $first = $true
        [bool] $separator = $false
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ($first) {
                $first = $false
            } else {
                if ($control -is [FortahSeparator]) {
                    $separator = $true
                } else {
                    if (( -not ($separator)) -and ($this.space -gt 0)) {
                        $renderables.Add([FortahNewLine]::new($this.space))
                    }
                    $separator = $false
                }
            }
            $control.appendToRenderables($renderables)
        }
        return $renderables
    }
    
}