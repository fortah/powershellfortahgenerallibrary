using module '.\FortahContainer.psm1'
using module '.\FortahControl.psm1'

using namespace System.Collections.Generic

class FortahWindow : FortahContainer {
    
    hidden [string] $userInput = $null
    [string] getUserInput() { return $this.userInput }

    FortahWindow() : base() {        
    }

    FortahWindow([List[FortahControl]] $pControls) : base($pControls) {
    }

    [void] render() {
        [List[FortahControl]] $renderables = $this.toRenderables()
        $this.userInput = ''
        [FortahControl] $renderable = $null
        foreach ($renderable in $renderables) {
            $renderable.render()
            [string] $renderableUserInput = $renderable.getUserInput()
            if (($null -ne $renderableUserInput) -and ($renderableUserInput.Length -gt 0)) {
                $this.userInput = $renderableUserInput
            }
        }
    }
}