using module '.\FortahControl.psm1'

using namespace System.Collections.Generic

class FortahSeparator : FortahControl {
    
    FortahSeparator() : base() {
    }
    
    FortahSeparator([string] $pName): base($pName) {
    }

    FortahSeparator([int] $pWidth) : base($pWidth) {        
    } 

    FortahSeparator([string] $pName, [int] $pWidth) : base($pName, $pWidth) {
    } 

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }    
    
}