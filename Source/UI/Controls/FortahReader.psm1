using module '.\FortahControl.psm1'
using module '..\..\General\FortahColour.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\General\FortahConsole.psm1'

using namespace System.Collections.Generic

class FortahReader : FortahControl {
    
    hidden [string] $prompt = ''
    [string] getPrompt() { return $this.prompt }
    [void] setPrompt([string] $pValue) { $this.prompt = $pValue }

    hidden [string] $userInput = ''
    [string] getUserInput() { return $this.userInput }

    FortahReader([string] $pPrompt) : base([FortahColours]::commandColour) {
        $this.prompt = $pPrompt
    }

    FortahReader([string] $pName, [string] $pPrompt) : base($pName, [FortahColours]::commandColour) {
        $this.prompt = $pPrompt
    }

    FortahReader([string] $pPrompt, [FortahColour] $pColour) : base($pColour) {
        $this.prompt = $pPrompt
    }

    FortahReader([string] $pName, [string] $pPrompt, [FortahColour] $pColour) : base($pName, $pColour) {
        $this.prompt = $pPrompt
    }

    static [FortahReader] createDefaultReader() {
        return [FortahReader]::new('Please, press "Enter" to continue')
    }
    
    static [FortahReader] createCommandReader() {
        return [FortahReader]::new('Please, type your command and press "Enter"')
    }

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }    
    
    [void] render() {
        $this.userInput = [FortahConsole]::readLine("$($this.prompt): ", $this.colour)
    }
}