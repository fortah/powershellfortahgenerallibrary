using module '.\FortahMenuCommand.psm1'
using module '.\FortahMenuItem.psm1'
using module '..\Controls\FortahControl.psm1'
using module '..\Controls\FortahFrame.psm1'
using module '..\Controls\FortahLabel.psm1'
using module '..\Controls\FortahSeparator.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\Runtime\FortahCommand.psm1'

using namespace System.Collections.Generic
using namespace System.Management.Automation

class FortahMenu : FortahFrame {
    
    hidden [string] $header = ''
    [string] getHeader() { return $this.header }
    [void] setHeader([string] $pValue) { $this.header = $pValue }

    hidden [List[FortahMenuItem]] $items = [List[FortahMenuItem]]::new()
    [List[FortahMenuItem]] getItems() { return $this.items }
    [void] setItems([List[FortahMenuItem]] $pValue) { $this.items = $pValue }

    FortahMenu([List[FortahMenuItem]] $pItems) : base() {
        $this.items = $pItems
    }

    FortahMenu([string] $pHeader, [List[FortahMenuItem]] $pItems) : base() {
        $this.header = $pHeader
        $this.items = $pItems
    }

    FortahMenu([int] $pWidth, [List[FortahMenuItem]] $pItems) : base($pWidth) {
        $this.items = $pItems
    }

    FortahMenu([int] $pWidth, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth) {
        $this.header = $pHeader
        $this.items = $pItems
    }

    [List[FortahControl]] toRenderables() {
        $this.controls = [List[FortahControl]]::new()
        $this.addHeaderControls()
        $this.addItemsControls()
        return ([FortahFrame]$this).toRenderables()
    }

    hidden [void] addHeaderControls() {
        if ($this.header.Length -gt 0) {
            $this.controls.Add([FortahLabel]::new($this.header, [FortahColours]::headerColour))
            $this.controls.Add([FortahSeparator]::new())
        }
    }

    hidden [void] addItemsControls() {
    }

    [FortahCommand] findCommand([string] $pKey) {
        [FortahCommand] $command = $null
        [FortahMenuItem] $item = ($this.items | Where-Object {(($_ -is [FortahMenuCommand]) -and ($_.getCommand().getKey() -eq $pKey))})
        if ($null -ne $item) {
            $command = $item.getCommand()
        }
        return $command
    }      
}
