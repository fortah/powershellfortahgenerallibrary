using module '.\FortahMenuItem.psm1'
using module '..\Controls\FortahControl.psm1'
using module '..\Controls\FortahLabel.psm1'
using module '..\Controls\FortahShelf.psm1'
using module '..\..\General\FortahColours.psm1'
using module '..\..\Runtime\FortahCommand.psm1'

using namespace System.Collections.Generic

class FortahMenuCommand : FortahMenuItem {

    hidden [FortahCommand] $command = [FortahCommand]::new()
    [FortahCommand] getCommand() { return $this.command }
    [void] setCommand([FortahCommand] $pValue) { $this.command = $pValue }

    FortahMenuCommand([FortahCommand] $pCommand) : base() {
        $this.command = $pCommand
    }

    FortahMenuCommand([string] $pCommandCode, [string] $pCommandKey, [string] $pCommandText) {
        $this.command = [FortahCommand]::new($pCommandCode, $pCommandKey, $pCommandText)
    }        

    [List[FortahControl]] toControls([int] $pCommandKeyLength) {
        return [FortahControl]::new([List[FortahControl]]::new(@([FortahShelf]::new([List[FortahControl]]::new(@(
            [FortahLabel]::new($this.command.getKey(), $pCommandKeyLength, [FortahColours]::commandColour),
            [FortahLabel]::new($this.command.getText(), [FortahColours]::selectedColour)
        ))))))
    }
}