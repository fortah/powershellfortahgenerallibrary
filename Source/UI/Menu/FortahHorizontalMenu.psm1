using module '.\FortahMenu.psm1'
using module '.\FortahMenuItem.psm1'
using module '..\Controls\FortahShelf.psm1'

using namespace System.Collections.Generic

class FortahHorizontalMenu : FortahMenu {
    
    hidden [int] $space = 1
    [int] getSpace() { return $this.space }
    [void] setSpace([int] $pValue) { $this.space = $pValue }

    FortahHorizontalMenu([List[FortahMenuItem]] $pItems) : base($pItems) {
    }

    FortahHorizontalMenu([string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pHeader, $pItems) {
    }

    FortahHorizontalMenu([int] $pWidth, [List[FortahMenuItem]] $pItems) : base($pWidth, $pItems) {
    }

    FortahHorizontalMenu([int] $pWidth, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth, $pHeader, $pItems) {
    }

    FortahHorizontalMenu([int] $pWidth, [int] $pSpace, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth, $pHeader, $pItems) {
        $this.space = $pSpace
    }

    [void] addItemsControls() {
        [FortahShelf] $shelf = [FortahShelf]::new($this.space)
        [FortahMenuItem] $item = $null
        foreach ($item in $this.items) {
            $shelf.addControls($item.toControls(0))
        }
        $this.controls.Add($shelf)
    }    
}