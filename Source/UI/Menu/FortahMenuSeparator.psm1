using module '.\FortahMenuItem.psm1'
using module '..\Controls\FortahControl.psm1'
using module '..\Controls\FortahSeparator.psm1'

using namespace System.Collections.Generic

class FortahMenuSeparator : FortahMenuItem {
    
    FortahMenuSeparator() : base() {
    }

    [List[FortahControl]] toControls([int] $pCommandKeyLength) {
        return [List[FortahControl]]::new(@([FortahSeparator]::new()))
    }    
    
}
