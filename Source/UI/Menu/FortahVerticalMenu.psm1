using module '.\FortahMenu.psm1'
using module '.\FortahMenuCommand.psm1'
using module '.\FortahMenuItem.psm1'
using module '..\Controls\FortahStack.psm1'

using namespace System.Collections.Generic

class FortahVerticalMenu : FortahMenu {
    
    FortahVerticalMenu([List[FortahMenuItem]] $pItems) : base($pItems) {
    }

    FortahVerticalMenu([string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pHeader, $pItems) {
    }

    FortahVerticalMenu([int] $pWidth, [List[FortahMenuItem]] $pItems) : base($pWidth, $pItems) {
    }

    FortahVerticalMenu([int] $pWidth, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth, $pHeader, $pItems) {
    }

    [void] addItemsControls() {
        [int] $commandKeyLength = $this.calculateCommandKeyLength()
        [FortahStack] $stack = [FortahStack]::new()
        [FortahMenuItem] $item = $null
        foreach ($item in $this.items) {
            $stack.addControls($item.toControls($commandKeyLength))
        }
        $this.controls.Add($stack)
    }    

    hidden [int] calculateCommandKeyLength() {
        [int] $commandKeyLength = 0
        [List[FortahMenuCommand]] $commandsItems = [List[FortahMenuCommand]]::new(($this.items | Where-Object {$_ -is [FortahMenuCommand]}))
        if ($null -ne $commandsItems) {
            [FortahMenuCommand] $commandItem = $null
            foreach ($commandItem in $commandsItems) {
                [int] $commandItemKeyLength = $commandItem.getCommand().getKey().Length 
                if ($commandItemKeyLength -gt $commandKeyLength) {
                    $commandKeyLength = $commandItemKeyLength
                }
            }
        }
        return $commandKeyLength
    }
    
}