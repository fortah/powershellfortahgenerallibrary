enum FortahLogMessageType {
    Null = 0
    Information = 1
    Warning = 2
    Error = 3
}
