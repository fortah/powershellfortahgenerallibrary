using module '.\FortahLogMessage.psm1'
using module '.\FortahLogMessageType.psm1'
using module '..\General\FortahPowerShell.psm1'

class FortahLogPropertyMessage : FortahLogMessage {

    FortahLogPropertyMessage([string] $pName, [Object] $pValue) : base([FortahLogPropertyMessage]::createText($pName, $pValue)) {
    }

    FortahLogPropertyMessage([FortahLogMessageType] $pType, [string] $pName, [Object] $pValue) : base($pType, [FortahLogPropertyMessage]::createText($pName, $pValue)) {
    }

    hidden static [string] createText([string] $pName, [Object] $pValue) {
        return "$($pName): $([FortahPowerShell]::toString($pValue))"
    }
    
}