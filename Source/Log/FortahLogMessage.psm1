using module '.\FortahLogColours.psm1'
using module '.\FortahLogItem.psm1'
using module '.\FortahLogItemType.psm1'
using module '.\FortahLogMessageType.psm1'
using module '..\General\FortahColour.psm1'
using module '..\General\FortahColours.psm1'

class FortahLogMessage : FortahLogItem {

    hidden [FortahLogMessageType] $messageType = [FortahLogMessageType]::null
    [FortahLogMessageType] getMessageType() { return $this.messageType }
    [void] setMessageType([FortahLogMessageType] $pValue) { $this.messageType = $pValue }

    hidden [string] $text = ''
    [string] getText() { return $this.text }
    [void] setText([string] $pValue) { $this.text = $pValue }

    FortahLogMessage() : base([FortahLogItemType]::message) {
    }

    FortahLogMessage([string] $pText) : base([FortahLogItemType]::message) {
        $this.FortahLogMessageEx([FortahLogMessageType]::information, $pText)
    }
    
    FortahLogMessage([FortahLogMessageType] $pMessageType, [string] $pText) : base([FortahLogItemType]::message) {
        $this.FortahLogMessageEx($pMessageType, $pText)
    }
    
    hidden [void] FortahLogMessageEx([FortahLogMessageType] $pMessageType, [string] $pText) {
        $this.setMessageType($pMessageType)
        $this.setText($pText)
    }

    [bool] isWarning() {
        return ($this.messageType -eq [FortahLogMessageType]::warning)
    }

    [bool] isError() {
        return ($this.messageType -eq [FortahLogMessageType]::error)
    }

    [bool] isErrorOrWarning() {
        return (($this.messageType -eq [FortahLogMessageType]::error) -or ($this.messageType -eq [FortahLogMessageType]::warning))
    }

    [FortahColour] getColour([FortahLogColours] $pColours) {
        [FortahColour] $colour = [FortahColours]::textColour
        switch ($this.messageType) {
            ([FortahLogMessageType]::information) {
                $colour = $pColours.getInformationColour()
            }
            ([FortahLogMessageType]::warning) {
                $colour = $pColours.getWarningColour()
            }
            ([FortahLogMessageType]::error) {
                $colour = $pColours.getErrorColour()
            }
        }
        return $colour
    }

    [string] toString() {
        [string] $string = ''
        switch ($this.messageType) {
            ([FortahLogMessageType]::warning) {
                $string = 'WARNING'
            }
            ([FortahLogMessageType]::warning) {
                $string = 'ERROR'
            }
        }
        if ($string -ne '') {
            $string += ': '
        }
        $string += $this.text
        return $string
    }
}