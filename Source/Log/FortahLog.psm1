using module '.\FortahLogItem.psm1'
using module '.\FortahLogItemType.psm1'
using module '.\FortahLogMessage.psm1'
using module '.\FortahLogMessageType.psm1'
using module '.\FortahLogPropertyMessage.psm1'
using module '..\General\FortahPowerShell.psm1'

using namespace System.Collections.Generic

class FortahLog : FortahLogItem {

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }

    hidden [List[FortahLogItem]] $items = [List[FortahLogItem]]::new()
    [List[FortahLogItem]] getItems() { return $this.items }
    [void] setItems([List[FortahLogItem]] $pValue) { $this.items = $pValue }

    FortahLog() : base([FortahLogItemType]::log) {
    }

    FortahLog([string] $pName) : base([FortahLogItemType]::log) {
        $this.setName($pName)
    }

    FortahLog([List[FortahLogItem]] $pItems) : base([FortahLogItemType]::log) {
        $this.add($pItems)
    }

    FortahLog([string] $pName, [List[FortahLogItem]] $pItems) : base([FortahLogItemType]::log) {
        $this.setName($pName)
        $this.add($pItems)
    }

    FortahLog([Hashtable] $pHash) : base([FortahLogItemType]::log) {
        $this.add($pHash)
    }

    FortahLog([string] $pName, [Hashtable] $pHash) : base([FortahLogItemType]::log) {
        $this.setName($pName)
        $this.add($pHash)
    }

    [void] addInformation([string] $pText) {
        $this.add([FortahLogMessageType]::information, $pText)
    }
    
    [void] addInformation([List[string]] $pTexts) {
        $this.add([FortahLogMessageType]::information, $pTexts)
    }

    [void] addWarning([string] $pText) {
        $this.add([FortahLogMessageType]::warning, $pText)
    }

    [void] addError([string] $pText) {
        $this.add([FortahLogMessageType]::error, $pText)
    }

    [void] add([FortahLogMessageType] $pType, [string] $pText) {
        $this.items.Add([FortahLogPropertyMessage]::new($pType, $pText))
    }

    [void] add([FortahLogMessageType] $pType, [List[string]] $pTexts) {
        [string] $text = ''
        foreach ($text in $pTexts) {
            $this.add($pType, $text)
        }
    }

    [void] add([string] $pName, [Object] $pValue) {
        $this.items.Add([FortahLogPropertyMessage]::new([FortahLogMessageType]::information, $pName, $pValue))
    }

    [void] add([FortahLogItem] $pItem) {
        $this.items.add($pItem)
    }

    [void] add([List[FortahLogItem]] $pItems) {
        [FortahLogItem] $item = $null
        foreach ($item in $pItems) {
            $this.items.add($item)
        }
    }

    [void] add([Hashtable] $pHash) {
        [string] $key = ''
        foreach ($key in $pHash.Keys) {
            $this.add($key, $pHash[$key])
        }
    }

    [void] testString([string] $pValue, [string] $pName) {
        $this.testStringEx($pValue, "$pName cannot be empty")
    }

    [void] testStringEx([string] $pValue, [string] $pText) {
        if ($pValue -eq '') {
            $this.addError($pText)
        }
    }

    [void] testInt([int] $pValue, [string] $pName) {
        $this.testIntEx($pValue, "$pName cannot be empty")
    }

    [void] testIntEx([int] $pValue, [string] $pText) {
        if ($pValue -le 0) {
            $this.addError($pText)
        }
    }

    [bool] hasWarnings() {
        [bool] $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::message) -and ($_.isWarning()))}))
        if (-not ($result)) {
            $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::log) -and ($_.hasWarnings()))}))
        }
        return $result
    }

    [bool] hasErrors() {
        [bool] $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::message) -and ($_.isError()))}))
        if (-not ($result)) {
            $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::log) -and ($_.hasErrors()))}))
        }
        return $result
    }

    [bool] hasErrorsOrWarnings() {
        [Bool] $result = $this.hasErrors()
        if (-not ($result)) {
            $result = $this.hasWarnings()
        }
        return $result
    }

    [void] throwExceptionIfHasErrors() {
        if ($this.hasErrors()) {
            throw $this.toString()
        }
    }

    [string] toString() {
        [string] $string = ''
        [FortahLogItem] $item = $null
        if ($this.name -ne '') {
            $string += "$($this.name): {"
        }
        foreach ($item in $this.items) {
            if ($string.Length -gt 0) {
                $string += " "
            }
            $string += $item.toString()
        }
        if ($this.name -ne '') {
            $string += '}'
        }
        return $string
    }
}
