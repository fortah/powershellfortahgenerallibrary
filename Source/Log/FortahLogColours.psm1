using module '..\General\FortahColour.psm1'
using module '..\General\FortahColours.psm1'
using module '..\General\FortahPowerShell.psm1'

class FortahLogColours {

    hidden [FortahColour] $titleColour = [FortahColour]::titleColour
    [FortahColour] getTitleColour() { return $this.titleColour }
    [void] setTitleColour([FortahColour] $pValue) { $this.titleColour = [FortahPowerShell]::notNull($pValue, $this.titleColour) }

    hidden [FortahColour] $informationColour = [FortahColours]::textColour
    [FortahColour] getInformationColour() { return $this.informationColour }
    [void] setInformationColour([FortahColour] $pValue) { $this.informationColour = [FortahPowerShell]::notNull($pValue, $this.informationColour) }
    
    hidden [FortahColour] $warningColour = [FortahColours]::warningColour
    [FortahColour] getWarningColour() { return $this.warningColour }
    [void] setWarningColour([FortahColour] $pValue) { $this.warningColour = [FortahPowerShell]::notNull($pValue, $this.warningColour) }

    hidden [FortahColour] $errorColour = [FortahColour]::errorColour
    [FortahColour] getErrorColour() { return $this.errorColour }
    [void] setErrorColour([FortahColour] $pValue) { $this.errorColour = [FortahPowerShell]::notNull($pValue, $this.errorColour) }

    LogColours() {
    }

    LogColours([FortahColour] $pTitleColour, [FortahColour] $pInformationColour, [FortahColour] $pWarningColour, [FortahColour] $pErrorColour) {
        $this.setTitleColour($pTItleColour)
        $this.setInformationColour($pInformationColour)
        $this.setWarningColour($pWarningColour)
        $this.setErrorColour($pErrorColour)
    }

}
