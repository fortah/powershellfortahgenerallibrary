using module '.\FortahLogItemType.psm1'

class FortahLogItem {

    hidden [FortahLogItemType] $type = [FortahLogItemType]::null
    [FortahLogItemType] getType() { return $this.type }
    [void] setType([FortahLogItemType] $pValue) { $this.type = $pValue }

    FortahLogItem([FortahLogItemType] $pType) {
        $this.setType($pType)
    }

}