using module '.\FortahLog.psm1'
using module '.\FortahLogColours.psm1'
using module '.\FortahLogItem.psm1'
using module '.\FortahLogItemType.psm1'
using module '..\General\FortahConsole.psm1'
using module '..\General\FortahString.psm1'

class FortahLogRenderer {

    hidden [FortahLogColours] $colours = [FortahLogColours]::new()
    [FortahLogColours] getColours() { return $this.colours }
    [void] setColours([FortahLogColours] $pValue) { $this.colours = $pValue }

    hidden [int] $tabSize = 2
    [int] getTabSize() { return $this.tabSize }
    [void] setTabSize([int] $pValue) { $this.tabSize = $pValue }

    FortahLogRenderer() {        
    }

    FortahLogRenderer([FortahLogColours] $pColours) {
        $this.FortahLogRendererEx($pColours)
    }

    hidden [void] FortahLogRendererEx([FortahLogColours] $pColours) {
        $this.setColours($pColours)
    }

    FortahLogRenderer([FortahLogColours] $pColours, [int] $pTabSize) {
        $this.FortahLogRendererEx($pColours, $pTabSize)
    }

    hidden [void] FortahLogRendererEx([FortahLogColours] $pColours, [int] $pTabSize) {
        $this.FortahLogRendererEx($pColours)
        $this.tabSize = $pTabSize
    }    

    [void] render([FortahLog] $pLog) {
        $this.render($pLog, 0)
    }

    [void] render([FortahLog] $pLog, [int] $pIndentation) {
        [int] $itemsIndentation = 0
        if ($pLog.getName() -ne '') {
            [string] $text = [FortahString]::indent($pLog.getName(), $pIndentation * $this.tabSize)
            [FortahConsole]::writeLine($text, $this.colours.getTitleColour())
            $itemsIndentation = 1
        }
        [FortahLogItem] $item = $null
        foreach ($item in $pLog.items) {
            switch ($item.getType()) {
                ([FortahLogItemType]::message) {
                    [string] $text = [FortahString]::indent($item.toString(), ($pIndentation + $itemsIndentation) * $this.tabSize)
                    [FortahConsole]::writeLine($text, $item.getColour($this.colours))
                }
                ([FortahLogItemType]::log) {
                    $this.render($item, $pIndentation + 1)
                }
            }
        }
    }
    
}