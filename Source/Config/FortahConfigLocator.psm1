using namespace System.IO

class FortahConfigLocator {

    static [string] findFilePath([string] $pFilePath) {
        [string] $filePath = ''
        if ([FortahConfigLocator]::isFilePathAFileName($pFilePath)) {
            $filePath = [Path]::Combine($PSScriptRoot, $pFilePath)
        } else {
            $filePath = $pFilePath
        }
        if (-not([File]::Exists($filePath))) {
            [string] $errorMessage = "Cannot find config file '$filePath'." 
            throw $errorMessage
        }
        return $filePath
    }

    hidden static [bool] isFilePathAFileName([string] $pFilePath) {
        return ([Path]::GetFileName($pFilePath) -eq $pFilePath)
    }

}