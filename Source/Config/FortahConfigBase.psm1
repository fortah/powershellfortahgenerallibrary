using module '.\FortahConfigLocator.psm1'

using namespace System.Management.Automation

class FortahConfigBase {

    static [string] $defaultFileName = 'config.json'

    FortahConfigBase() {
    }

    [void] read() {
        $this.read('')
    }

    [void] read([string] $pFilePath) {
        [string] $filePath = [FortahConfigLocator]::findFilePath($pFilePath)
        [PSCustomObject] $json = (Get-Content -Path $filePath -Raw | ConvertFrom-Json)
        $this.readInt($json)
    }

    [void] readInt([PSCustomObject] $pJson) {
    }

}