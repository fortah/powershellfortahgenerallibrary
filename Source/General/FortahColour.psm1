using module '.\FortahPowerShell.psm1'

using namespace System

enum FortahColour {
    null = -1
    black =	0 	
    darkBlue = 1 	
    darkGreen = 2 	
    darkCyan = 3
    darkRed = 4 	
    darkMagenta = 5 	
    darkYellow = 6 	
    grey = 7 	
    darkGrey = 8 	
    blue = 9 	
    green = 10 	
    cyan = 11 	
    red = 12 	
    magenta = 13 	
    yellow = 14 	
    white = 15
}

class FortahColourEx {

    static [ConsoleColor] toSystemConsoleColor([FortahColour] $pConsoleColour) {
        [ConsoleColor] $systemConsoleColor = [ConsoleColor]::Black
        switch ($pConsoleColour) {
            ([FortahColour]::darkBlue) {
                $systemConsoleColor = [ConsoleColor]::DarkBlue
            }
            ([FortahColour]::darkGreen) {
                $systemConsoleColor = [ConsoleColor]::DarkGreen
            } 	
            ([FortahColour]::darkCyan) {
                $systemConsoleColor = [ConsoleColor]::DarkCyan
            }
            ([FortahColour]::darkRed) {
                $systemConsoleColor = [ConsoleColor]::DarkRed
            } 	
            ([FortahColour]::darkMagenta) {
                $systemConsoleColor = [ConsoleColor]::DarkMagenta
            } 	
            ([FortahColour]::darkYellow) {
                $systemConsoleColor = [ConsoleColor]::DarkYellow
            } 	
            ([FortahColour]::grey) {
                $systemConsoleColor = [ConsoleColor]::Gray
            } 	
            ([FortahColour]::darkGrey) {
                $systemConsoleColor = [ConsoleColor]::DarkGray
            } 	
            ([FortahColour]::blue) {
                $systemConsoleColor = [ConsoleColor]::Blue
            } 	
            ([FortahColour]::green) {
                $systemConsoleColor = [ConsoleColor]::Green
            } 	
            ([FortahColour]::cyan) {
                $systemConsoleColor = [ConsoleColor]::Cyan
            } 	
            ([FortahColour]::red) {
                $systemConsoleColor = [ConsoleColor]::Red
            } 	
            ([FortahColour]::magenta) {
                $systemConsoleColor = [ConsoleColor]::Magenta
            } 	
            ([FortahColour]::yellow) {
                $systemConsoleColor = [ConsoleColor]::Yellow
            } 	
            ([FortahColour]::white) {
                $systemConsoleColor = [ConsoleColor]::White
            }            
        }
        return $systemConsoleColor
    }

    static [bool] isNull([FortahColour] $pColour) {
        return ($pColour -eq [FortahColour]::null)
    }

    static [bool] isNotNull([FortahColour] $pColour) {
        return ($pColour -ne [FortahColour]::null)
    }

    static [FortahColour] notNull([FortahColour] $pPrimaryColour, [FortahColour] $pSecondaryColour) {
        return [FortahPowerShell]::iif([FortahColourEx]::isNotNull($pPrimaryColour), $pPrimaryColour, $pSecondaryColour)
    }

}
