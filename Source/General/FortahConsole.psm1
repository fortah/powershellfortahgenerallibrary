using module '.\FortahColour.psm1'
using module '.\FortahPowerShell.psm1'

class FortahConsole {

    static [void] clearScreen() {
        Clear-Host
    }

    static [void] newLine() {
        Write-Host
    }

    static [void] write([Object] $pValue) {
        [FortahConsole]::write($pValue, [FortahColour]::null)
    }

    static [void] writeLine([Object] $pValue) {
        [FortahConsole]::writeLine($pValue, [FortahColour]::null)
    }

    static [void] write([Object] $pValue, [FortahColour] $pColour) {
        [string] $text = [FortahPowerShell]::toString($pValue)
        if ([FortahColourEx]::isNotNull($pColour)) {
            Write-Host ($text) -ForegroundColor ([FortahColourEx]::toSystemConsoleColor($pColour)) -NoNewline
        } else {
            Write-Host ($text) -NoNewline
        }
    }

    static [void] writeLine([Object] $pValue, [FortahColour] $pColour) {
        [FortahConsole]::write($pValue, $pColour)
        [FortahConsole]::newLine()
    }

    static [string] readLine([string] $pPrompt) {
        return [FortahConsole]::readLine($pPrompt, [FortahColour]::null)
    }

    static [string] readLine([string] $pPrompt, [FortahColour] $pColour) {
        [FortahConsole]::write($pPrompt, $pColour)
        return Read-Host
    }    
    
}
