using module '.\FortahInt.psm1'

using namespace System.Collections.Generic

class FortahVersion {

    hidden [int] $major = 0
    [int] getMajor() { return $this.major }
    [void] setMajor([int] $pValue) { $this.major = $pValue }

    hidden [int] $minor = 0
    [int] getMinor() { return $this.minor }
    [void] setMinor([int] $pValue) { $this.minor = $pValue }

    hidden [int] $hotfix = 0
    [int] getHotfix() { return $this.hotfix }
    [void] setHotfix([int] $pValue) { $this.hotfix = $pValue }

    hidden [int] $build = 0
    [int] getBuild() { return $this.build }
    [void] setBuild([int] $pValue) { $this.build = $pValue }

    FortahVersion() {
    }

    FortahVersion([int] $pMajor, [int] $pMinor) {
        $this.FortahVersionEx($pMajor, $pMinor)
    }

    hidden [void] FortahVersionEx([int] $pMajor, [int] $pMinor) {
        $this.setMajor($pMajor)
        $this.setMinor($pMinor)
    }

    FortahVersion([int] $pMajor, [int] $pMinor, [int] $pHotfix) {
        $this.FortahVersionEx($pMajor, $pMinor, $pHotfix)
    }

    hidden [void] FortahVersionEx([int] $pMajor, [int] $pMinor, [int] $pHotfix) {
        $this.FortahVersionEx($pMajor, $pMinor)
        $this.setHotfix($pHotfix)
    }

    FortahVersion([int] $pMajor, [int] $pMinor, [int] $pHotfix, [int] $pBuild) {
        $this.FortahVersionEx($pMajor, $pMinor, $pHotfix, $pBuild)
    }

    hidden [void] FortahVersionEx([int] $pMajor, [int] $pMinor, [int] $pHotfix, [int] $pBuild) {
        $this.FortahVersionEx($pMajor, $pMinor, $pHotfix)
        $this.setBuild($pBuild)
    }

    [bool] isEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -eq 0);
    }

    [bool] isNotEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -ne 0);
    }

    [bool] isLower([Version] $pVersion) {
        return ($this.compare($pVersion) -lt 0);
    }

    [bool] isLowerOrEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -le 0);
    }

    [bool] isGreater([Version] $pVersion) {
        return ($this.compare($pVersion) -gt 0);
    }

    [bool] isGreaterOrEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -ge 0);
    }

    [int] compare([Version] $pVersion) {
        [int] $result = [FortahInt]::compare($this.major, $pVersion.getMajor());
        if ($result -eq 0) {
            $result = [FortahInt]::compare($this.minor, $pVersion.getMinor());
        }
        if ($result -eq 0) {
            $result = [FortahInt]::compare($this.hotfix, $pVersion.getHotfix());
        }       
        if ($result -eq 0) {
            $result = [FortahInt]::compare($this.build, $pVersion.getBuild());
        }
        return $result;
    }

    [string] toString() {
        return "$($this.major).$($this.minor).$($this.hotfix).$($this.build)"
    }

    static [Version] parse([string] $pText) {
        [Version] $version = [Version]::new()
        [List[string]] $textParts = $pText.Split('.')
        if ($textParts.Length -eq 4) {
            $version.setMajor([int]::parse($textParts[0]))
            $version.setMinor([int]::parse($textParts[1]))
            $version.setHotfix([int]::parse($textParts[2]))
            $version.setBuild([int]::parse($textParts[3]))
        } else {
            throw "Text '$pText' does not represent a version."
        }
        return $version
    }
}
