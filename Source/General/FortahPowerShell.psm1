using namespace System
using namespace System.Collections

class FortahPowerShell {

    static [bool] isNull([Object] $pValue) {
        return ($null -eq $pValue)
    }

    static [bool] isNotNull([Object] $pValue) {
        return ($null -ne $pValue)
    }

    static [Object] iif ([bool] $pCondition, [Object] $pValueIfTrue, [Object] $pValueIfFalse) {
        if ($pCondition) {
            return $pValueIfTrue
        } else {
            return $pValueIfFalse
        }
    }

    static [Object] notNull ([Object] $pPrimaryValue, [Object] $pSecondaryValue) {
        if ([FortahPowerShell]::isNotNull($pPrimaryValue)) {
            return $pPrimaryValue
        } else {
            return $pSecondaryValue
        }
    }    

    static [bool] isStringEmpty([string] $pString) {
        return (([FortahPowerShell]::isNull($pString)) -or ($pString.Length -eq 0))
    }

    static [bool] isStringNotEmpty([string] $pString) {
        return (([FortahPowerShell]::isNotNull($pString)) -and ($pString.Length -gt 0))
    }

    static [string] nonEmptyString([string] $pString1, [string] $pString2) {
        return [FortahPowerShell]::iif([FortahPowerShell]::isStringNotEmpty($pString1), $pString1, $pString2)
    }

    static [string] toString([Object] $pValue) {
        if ([FortahPowerShell]::isNotNull($pValue)) {
            return $pValue.ToString()
        } else {
            return 'null'
        }
    }
    
}
