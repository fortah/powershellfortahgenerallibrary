using module '.\FortahColour.psm1'
using module '.\FortahState.psm1'

class FortahColours {

    static [FortahColour] $titleColour = [FortahColour]::green
    static [FortahColour] $headerColour = [FortahColour]::green
    static [FortahColour] $borderColour = [FortahColour]::grey

    static [FortahColour] $textColour = [FortahColour]::white
    static [FortahColour] $selectedColour = [FortahColour]::yellow
    static [FortahColour] $inactiveColour = [FortahColour]::grey

    static [FortahColour] $commandColour = [FortahColour]::cyan
    static [FortahColour] $debugColour = [FortahColour]::magenta

    static [FortahColour] $warningColour = [FortahColour]::magenta
    static [FortahColour] $errorColour = [FortahColour]::red

    static [FortahColour] getStateColour([FortahState] $pState) {
        [FortahColour] $stateColour = [FortahColours]::textColour
        switch ($pState) {
            ([FortahState]::selected) {
                $stateColour = [FortahColours]::selectedColour
            }
            ([FortahState]::inactive) {
                $stateColour = [FortahColours]::inactiveColour
            }
        }
        return $stateColour
    }
}