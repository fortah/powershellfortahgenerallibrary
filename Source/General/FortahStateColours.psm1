using module '.\FortahColour.psm1'
using module '.\FortahColours.psm1'
using module '.\FortahPowerShell.psm1'
using module '.\FortahState.psm1'

class FortahStateColours {

    hidden [FortahColour] $normalColour = [FortahColours]::textColour
    [FortahColour] getNormalColour() { return $this.normalColour }
    [void] setNormalColour([FortahColour] $pValue) { $this.normalColour = [FortahColourEx]::notNull($pValue, $this.normalColour) }

    hidden [FortahColour] $selectedColour = [FortahColours]::selectedColour
    [FortahColour] getSelectedColour() { return $this.selectedColour }
    [void] setSelectedColour([FortahColour] $pValue) { $this.selectedColour = [FortahColourEx]::notNull($pValue, $this.selectedColour) }

    hidden [FortahColour] $inactiveColour = [FortahColours]::inactiveColour
    [FortahColour] getInactiveColour() { return $this.inactiveColour }
    [void] setInactiveColour([FortahColour] $pValue) { $this.inactiveColour = [FortahColourEx]::notNull($pValue, $this.inactiveColour) }    

    StateColours() {
    }
    
    StateColours([FortahColour] $pNormalColour, [FortahColour] $pSelectedColour, [FortahColour] $pInactiveColour) {
        $this.setNormalColour($pNormalColour)
        $this.setSelectedColour($pSelectedColour)
        $this.setInactiveColour($pInactiveColour)
    }

    [string] getColour([FortahState] $pState) {
        [FortahColour] $colour = $this.normalColour
        switch ($pState) {
            ([FortahState]::selected) {
                $colour = $this.selectedColour
            }
            ([FortahState]::inactive) {
                $colour = $this.inactiveColour
            }
        }
        return $colour
    }    
    
}