using namespace System

class FortahSub {

    hidden [Object] $super = $null
    [Object] getSuper() { return $this.super }
    [void] setSuper([Object] $pValue) { $this.super = $pValue }

    Sub() {
    }

    Sub([Object] $pSuper) {
        $this.setSuper($pSuper)
    }
    
}