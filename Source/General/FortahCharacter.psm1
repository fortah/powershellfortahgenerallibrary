class FortahCharacter {

    static [char] $horizontal = [char]0x2500;
    static [char] $vertical = [char]0x2502;
    static [char] $topLeft = [char]0x250C;
    static [char] $topRight = [char]0x2510;
    static [char] $bottomLeft = [char]0x2514;
    static [char] $bottomRight = [char]0x2518;
    static [char] $leftJoin = [char]0x251C;
    static [char] $rightJoin = [char]0x2524;
    static [char] $topJoin = [char]0x252C;
    static [char] $bottomJoin = [char]0x2534;
    static [char] $on = [char]0x25A0;
    static [char] $off = [char]0x25A1;

}