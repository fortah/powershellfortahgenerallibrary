class FortahString {

    static [string] indent([string] $pString, [int] $pIndentation) {
        [string] $prefix = ''
        if ($pIndentation -gt 0) {
            $prefix = (' ' * $pIndentation)
        }
        return $prefix + $pString
    }
    
}