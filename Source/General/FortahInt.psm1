class FortahInt {

    static [bool] compare([int] $pValue1, [int] $pValue2) {
        if ($pValue1 -gt $pValue2) {
            return 1
        } else { 
            if ($pValue1 -lt $pValue2) {
                return -1
            } else {
                return 0
            }
        }
    }
    
}
