using module '.\FortahCommand.psm1'
using module '.\FortahHandler.psm1'
using module '.\FortahLoopState.psm1'
using module '..\General\FortahColours.psm1'
using module '..\General\FortahConsole.psm1'

using module '..\UI\Controls\Window.psm1'

using namespace System.Collections

class FortahLoop {
    hidden [FortahWindow] $window = $null
    [FortahWindow] getWindow() { return $this.window }

    hidden [FortahHandler] $handler = $null
    [FortahHandler] getHandler() { return $this.handler }

    hidden [bool] $running = $false
    [bool] isRunning() { return $this.running }

    FortahLoop() {        
    }

    [FortahWindow] createWindow() {
        return $null
    }

    [FortahHandler] createHandler() {
        return $null
    }

    [void] run() {
        $this.start()
        while ($this.running) {
            [string] $userInput = $this.render([FortahLoopState]::userInput)
            [FortahCommand] $command = $this.findCommand($userInput)
            $this.handle($command)
        }
        $this.finish()
    }
    
    hidden [void] start() {
        $this.window = $this.createWindow()
        $this.handler = $this.createHandler()
        $this.running = (($null -ne $this.window) -and ($null -ne $this.handler))
    }

    hidden [string] render([FortahLoopState] $pLoopState) {
        $this.updateWindow($pLoopState)
        [FortahConsole]::clearScreen() 
        $this.window.render()
        return $this.window.getUserInput()
    }

    hidden [void] updateWindow([FortahLoopState] $pLoopState) {        
    }

    hidden [FortahCommand] findCommand([string] $pKey) {
        [FortahCommand] $command = $this.window.findCommand($pKey)
        if ($null -eq $command) {
            [FortahConsole]::readLine("Unknown command: ""$($pKey)"". Please, press ""Enter"" and try again: ", [FortahColours]::errorColour)
        }
        return $command
    }

    hidden [void] handle([FortahCommand] $pCommand) {
        if ($null -ne $pCommand) {
            $this.render([FortahLoopState]::handling)
            $this.handler.handle($pCommand)
        }
    }

    [void] stop() {
        $this.running = $false
    }

    hidden [void] finish() {
    }

    [void] exit() {
        $this.running = $false
    }    
}