class FortahCommand {

    hidden [string] $code = ''
    [string] getCode() { return $this.code }

    hidden [string] $key = ''
    [string] getKey() { return $this.key }

    hidden [string] $text = ''
    [string] getText() { return $this.text }

    FortahCommand() {
    }

    FortahCommand([string] $pCode, [string] $pKey) {
        $this.FortahCommandEx($pCode, $pKey, $pKey)
    }

    FortahCommand([string] $pCode, [string] $pKey, [string] $pText) {
        $this.FortahCommandEx($pCode, $pKey, $pText)
    }    

    hidden [void] FortahCommandEx([string] $pCode, [string] $pKey, [string] $pText) {
        $this.code = $pCode
        $this.key = $pKey
        $this.text = $pText
    }

    [string] toString() {
        return $this.text
    }
}