using module '..\General\FortahVersion.psm1'

class FortahManifest {

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }
    
    hidden [string] $description = ''
    [string] getDescription() { return $this.description }
    [void] setDescription([string] $pValue) { $this.description = $pValue }

    hidden [FortahVersion] $version = [FortahVersion]::new()
    [FortahVersion] getVersion() { return $this.version }
    [void] setVersion([FortahVersion] $pValue) { $this.version = $pValue }

    hidden [string] $company = ''
    [string] getCompany() { return $this.company }
    [void] setCompany([string] $pValue) { $this.company = $pValue }

    hidden [string] $author = ''
    [string] getAuthor() { return $this.author }
    [void] setAuthor([string] $pValue) { $this.author = $pValue }

    hidden [string] $date = ''
    [string] getDate() { return $this.date }
    [void] setDate([string] $pValue) { $this.date = $pValue }

    FortahManifest([string] $pName, [string] $pDescription, [FortahVersion] $pVersion, [string] $pCompany, [string] $pAuthor, [string] $pDate) {
        $this.setName($pName)
        $this.setDescription($pDescription)
        $this.setVersion($pVersion)
        $this.setCompany($pCompany)
        $this.setAuthor($pAuthor)
        $this.setDate($pDate)
    }

    [string] toString() {
        return "$($this.company) - $($this.name) - $($this.version.toString()) - $($this.author) - $($this.date)"
    }

    [string] toTitleLine1() {
        return "$($this.company) - $($this.name) - $($this.version.toString())"
    }

    [string] toTitleLine2() {
        return "$($this.author) - $($this.date)"
    }
}
