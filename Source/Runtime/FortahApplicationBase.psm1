using module '.\FortahLoop.psm1'
using module '.\FortahManifest.psm1'
using module '..\Config\FortahConfigBase.psm1'
using module '..\General\FortahColours.psm1'
using module '..\General\FortahConsole.psm1'

using namespace System.Collections.ObjectModel

class FortahApplicationBase {

    hidden [FortahManifest] $manifest = $null
    [FortahManifest] getManifest() { return $this.manifest }

    hidden [FortahConfigBase] $config = $null
    [FortahConfigBase] getConfig() { return $this.config }
    
    hidden [FortahLoop] $loop = $null
    [FortahLoop] getLoop() { return $this.loop }

    FortahApplicationBase([FortahManifest] $pManifest) {
        $this.manifest = $pManifest
    }

    FortahApplicationBase([FortahManifest] $pManifest, [bool] $pReadConfig) {
        $this.manifest = $pManifest
        if ($pReadConfig) {
            $this.readConfig('')
        }
    }

    FortahApplicationBase([FortahManifest] $pManifest, [string] $pConfigFilePath) {
        $this.manifest = $pManifest
        $this.readConfig($pConfigFilePath)
    }

    FortahApplicationBase([FortahManifest] $pManifest, [FortahConfigBase] $pConfig) {
        $this.manifest = $pManifest
        $this.config = $pConfig
    }

    hidden [void] readConfig([string] $pConfigFilePath) {
        $this.config = $this.createConfig() 
        if ($null -ne $this.config) {
            $this.config.read($pConfigFilePath)
        }
    }

    [FortahConfigBase] createConfig() {
        return [FortahConfigBase]::new()
    }

    [FortahLoop] createLoop() {
        return $null
    }

    [void] run() {
        $this.start()
        if ($null -ne $this.loop) {
            $this.loop.run()
        }
        $this.finish()
    }

    [void] start() {
        $this.loop = $this.createLoop()
    }

    [void] finish() {
        [FortahConsole]::newLine()
        [FortahConsole]::writeLine('Good bye!', [FortahColours]::selectedColour)
        [FortahConsole]::newLine()
    }

}
