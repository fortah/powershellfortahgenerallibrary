class FortahHandlingOptions {

    hidden [string] $initialMessage = ''
    [string] getInitialMessage() { return $this.initialMessage }
    [void] setInitialMessage([string] $pValue) { $this.initialMessage = $pValue }
    
    hidden [string] $finalMessage = ''
    [string] getFinalMessage() { return $this.finalMessage }
    [void] setFinalMessage([string] $pValue) { $this.finalMessage = $pValue }

    hidden [bool] $onlyFirstResult = $false
    [bool] isOnlyFirstResult() { return $this.onlyFirstResult }
    [void] setOnlyFirstResult([bool] $pValue) { $this.onlyFirstResult = $pValue }
    
    FortahHandlingOptions([string] $pInitialMessage, [string] $pFinalMessage) {
        $this.FortahHandlingOptionsEx($pInitialMessage, $pFinalMessage)
    }

    hidden [void] FortahHandlingOptionsEx([string] $pInitialMessage, [string] $pFinalMessage) {
        $this.setInitialMessage($pInitialMessage)
        $this.setFinalMessage($pFinalMessage)
    }

    FortahHandlingOptions([string] $pInitialMessage, [string] $pFinalMessage, [bool] $pOnlyFirstResult) {
        $this.FortahHandlingOptionsEx($pInitialMessage, $pFinalMessage)
        $this.setOnlyFirstResult($pOnlyFirstResult)
    }

    FortahHandlingOptions([string] $pPrefix) {
        $this.FortahHandlingOptionsEx($pPrefix, 'in progress...', 'completed.')
    }

    FortahHandlingOptions([string] $pPrefix, [bool] $pOnlyFirstResult) {
        $this.FortahHandlingOptionsEx($pPrefix, 'in progress...', 'completed.')
        $this.setOnlyFirstResult($pOnlyFirstResult)
    }

    FortahHandlingOptions([string] $pPrefix, [string] $pInitialMessageSuffix, [string] $pFinalMessageSuffix) {
        $this.FortahHandlingOptionsEx($pPrefix, $pInitialMessageSuffix, $pFinalMessageSuffix)
    }

    FortahHandlingOptions([string] $pPrefix, [string] $pInitialMessageSuffix, [string] $pFinalMessageSuffix, [bool] $pOnlyFirstResult) {
        $this.FortahHandlingOptionsEx($pPrefix, $pInitialMessageSuffix, $pFinalMessageSuffix)
        $this.setOnlyFirstResult($pOnlyFirstResult)
    }

    hidden [void] FortahHandlingOptionsEx([string] $pPrefix, [string] $pInitialMessageSuffix, [string] $pFinalMessageSuffix) {
        if (($pPrefix -ne '') -and ($pInitialMessageSuffix -ne '')) {
            $this.setInitialMessage($pPrefix + ' ' + $pInitialMessageSuffix)
        }
        if (($pPrefix -ne '') -and ($pFinalMessageSuffix -ne '')) {
            $this.setFinalMessage($pPrefix + ' ' + $pFinalMessageSuffix)
        }
    }
}
