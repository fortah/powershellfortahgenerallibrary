class FortahHandlingResult {

    hidden [bool] $handled = $false
    [bool] isHandled() { return $this.handled }
    [void] setHandled([bool] $pValue) { $this.handled = $pValue }

    hidden [bool] $followedByEnter = $false
    [bool] isFollowedByEnter() { return $this.followedByEnter }
    [void] setFollowedByEnter([bool] $pValue) { $this.followedByEnter = $pValue }

    FortahHandlingResult() {
    }

    FortahHandlingResult([bool] $pHandled) {
        $this.setHandled($pHandled)
    }

    FortahHandlingResult([bool] $pHandled, [bool] $pFollowedByEnter) {
        $this.setHandled($pHandled)
        $this.setFollowedByEnter($pFollowedByEnter)
    }

    [void] setTrue() {
        $this.setHandled($true)
        $this.setFollowedByEnter($true)
    }

    [void] setHandledTrue() {
        $this.setHandled($true)
    }

}