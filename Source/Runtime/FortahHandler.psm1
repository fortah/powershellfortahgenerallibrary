using module '.\FortahCommand.psm1'
using module '.\FortahHandlingOptions.psm1'
using module '..\General\FortahColour.psm1'
using module '..\General\FortahColours.psm1'
using module '..\General\FortahConsole.psm1'
using module '..\General\FortahPowerShell.psm1'
using module '..\General\FortahSub.psm1'

using namespace System
using namespace System.Collections.ObjectModel
using namespace System.Management.Automation

class FortahHandler : FortahSub {
    
    hidden [Object] getLoop() { return $this.super() }
    
    FortahHandler([Object] $pSuper) : base($pSuper) {
    }

    [void] handle([FortahCommand] $pCommand) {
    }

    hidden [Object] run([ScriptBlock] $pCode, [FortahHandlingOptions] $pOptions) {
        return $this.run($pCode, $null, $pOptions)
    }

    hidden [Object] run([ScriptBlock] $pCode, [Object] $pArguments, [FortahHandlingOptions] $pOptions) {
        [Object] $result = $null

        $this.renderMessage($pOptions.getInitialMessage(), $false, $false)
        
        [Collection`1[PSObject]] $invokeResult = $null

        if ([FortahPowerShell]::isNotNull($pArguments)) {
            $invokeResult = $pCode.Invoke($this, $pArguments)
        } else {
            $invokeResult = $pCode.Invoke($this)
        }

        if ([FortahPowerShell]::isNotNull($invokeResult)) {
            if ($pOptions.isOnlyFirstResult()) {
                $result = $invokeResult[0]
            } else {
                $result = $invokeResult
            }
        }

        $this.renderMessage($pOptions.getFinalMessage(), $false, $false)

        return $result
    }

    hidden [void] renderMessage([string] $pMessage, [bool] $pNewLineBefore, [bool] $pNewLineAfter) {
        if ($pNewLineBefore) {
            [FortahConsole]::newLine()
        }
        if ([FortahPowerShell]::isStringNotEmpty($pMessage)) {
            [FortahConsole]::writeLine($pMessage, [FortahColours]::textColour)
        }
        if ($pNewLineAfter) {
            [FortahConsole]::newLine()
        }
    }

    hidden [void] pretend([string] $pDescription, [string] $pClass, [string] $pMethod) {
        [FortahConsole]::writeLine("$pDescription --> [$pClass]::$pMethod()", [FortahColours]::debugColour)
    }
    
}