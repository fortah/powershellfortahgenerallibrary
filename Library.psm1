using namespace System
using namespace System.Collections
using namespace System.Collections.Generic
using namespace System.Management.Automation
using namespace System.IO
using namespace System.Collections.ObjectModel

class FortahPowerShell {

    static [bool] isNull([Object] $pValue) {
        return ($null -eq $pValue)
    }

    static [bool] isNotNull([Object] $pValue) {
        return ($null -ne $pValue)
    }

    static [Object] iif ([bool] $pCondition, [Object] $pValueIfTrue, [Object] $pValueIfFalse) {
        if ($pCondition) {
            return $pValueIfTrue
        } else {
            return $pValueIfFalse
        }
    }

    static [Object] notNull ([Object] $pPrimaryValue, [Object] $pSecondaryValue) {
        if ([FortahPowerShell]::isNotNull($pPrimaryValue)) {
            return $pPrimaryValue
        } else {
            return $pSecondaryValue
        }
    }    

    static [bool] isStringEmpty([string] $pString) {
        return (([FortahPowerShell]::isNull($pString)) -or ($pString.Length -eq 0))
    }

    static [bool] isStringNotEmpty([string] $pString) {
        return (([FortahPowerShell]::isNotNull($pString)) -and ($pString.Length -gt 0))
    }

    static [string] nonEmptyString([string] $pString1, [string] $pString2) {
        return [FortahPowerShell]::iif([FortahPowerShell]::isStringNotEmpty($pString1), $pString1, $pString2)
    }

    static [string] toString([Object] $pValue) {
        if ([FortahPowerShell]::isNotNull($pValue)) {
            return $pValue.ToString()
        } else {
            return 'null'
        }
    }
    
}

class FortahInt {

    static [bool] compare([int] $pValue1, [int] $pValue2) {
        if ($pValue1 -gt $pValue2) {
            return 1
        } else { 
            if ($pValue1 -lt $pValue2) {
                return -1
            } else {
                return 0
            }
        }
    }
    
}

class FortahString {

    static [string] indent([string] $pString, [int] $pIndentation) {
        [string] $prefix = ''
        if ($pIndentation -gt 0) {
            $prefix = (' ' * $pIndentation)
        }
        return $prefix + $pString
    }
    
}

enum FortahColour {
    null = -1
    black =	0 	
    darkBlue = 1 	
    darkGreen = 2 	
    darkCyan = 3
    darkRed = 4 	
    darkMagenta = 5 	
    darkYellow = 6 	
    grey = 7 	
    darkGrey = 8 	
    blue = 9 	
    green = 10 	
    cyan = 11 	
    red = 12 	
    magenta = 13 	
    yellow = 14 	
    white = 15
}

class FortahColourEx {

    static [ConsoleColor] toSystemConsoleColor([FortahColour] $pConsoleColour) {
        [ConsoleColor] $systemConsoleColor = [ConsoleColor]::Black
        switch ($pConsoleColour) {
            ([FortahColour]::darkBlue) {
                $systemConsoleColor = [ConsoleColor]::DarkBlue
            }
            ([FortahColour]::darkGreen) {
                $systemConsoleColor = [ConsoleColor]::DarkGreen
            } 	
            ([FortahColour]::darkCyan) {
                $systemConsoleColor = [ConsoleColor]::DarkCyan
            }
            ([FortahColour]::darkRed) {
                $systemConsoleColor = [ConsoleColor]::DarkRed
            } 	
            ([FortahColour]::darkMagenta) {
                $systemConsoleColor = [ConsoleColor]::DarkMagenta
            } 	
            ([FortahColour]::darkYellow) {
                $systemConsoleColor = [ConsoleColor]::DarkYellow
            } 	
            ([FortahColour]::grey) {
                $systemConsoleColor = [ConsoleColor]::Gray
            } 	
            ([FortahColour]::darkGrey) {
                $systemConsoleColor = [ConsoleColor]::DarkGray
            } 	
            ([FortahColour]::blue) {
                $systemConsoleColor = [ConsoleColor]::Blue
            } 	
            ([FortahColour]::green) {
                $systemConsoleColor = [ConsoleColor]::Green
            } 	
            ([FortahColour]::cyan) {
                $systemConsoleColor = [ConsoleColor]::Cyan
            } 	
            ([FortahColour]::red) {
                $systemConsoleColor = [ConsoleColor]::Red
            } 	
            ([FortahColour]::magenta) {
                $systemConsoleColor = [ConsoleColor]::Magenta
            } 	
            ([FortahColour]::yellow) {
                $systemConsoleColor = [ConsoleColor]::Yellow
            } 	
            ([FortahColour]::white) {
                $systemConsoleColor = [ConsoleColor]::White
            }            
        }
        return $systemConsoleColor
    }

    static [bool] isNull([FortahColour] $pColour) {
        return ($pColour -eq [FortahColour]::null)
    }

    static [bool] isNotNull([FortahColour] $pColour) {
        return ($pColour -ne [FortahColour]::null)
    }

    static [FortahColour] notNull([FortahColour] $pPrimaryColour, [FortahColour] $pSecondaryColour) {
        return [FortahPowerShell]::iif([FortahColourEx]::isNotNull($pPrimaryColour), $pPrimaryColour, $pSecondaryColour)
    }

}

class FortahColours {

    static [FortahColour] $titleColour = [FortahColour]::green
    static [FortahColour] $headerColour = [FortahColour]::green
    static [FortahColour] $borderColour = [FortahColour]::grey

    static [FortahColour] $textColour = [FortahColour]::white
    static [FortahColour] $selectedColour = [FortahColour]::yellow
    static [FortahColour] $inactiveColour = [FortahColour]::grey

    static [FortahColour] $commandColour = [FortahColour]::cyan
    static [FortahColour] $debugColour = [FortahColour]::magenta

    static [FortahColour] $warningColour = [FortahColour]::magenta
    static [FortahColour] $errorColour = [FortahColour]::red

    static [FortahColour] getStateColour([FortahState] $pState) {
        [FortahColour] $stateColour = [FortahColours]::textColour
        switch ($pState) {
            ([FortahState]::selected) {
                $stateColour = [FortahColours]::selectedColour
            }
            ([FortahState]::inactive) {
                $stateColour = [FortahColours]::inactiveColour
            }
        }
        return $stateColour
    }
}

enum FortahState {
    null = 0
    normal = 1
    selected = 2
    inactive = 3
}

class FortahStateColours {

    hidden [FortahColour] $normalColour = [FortahColours]::textColour
    [FortahColour] getNormalColour() { return $this.normalColour }
    [void] setNormalColour([FortahColour] $pValue) { $this.normalColour = [FortahColourEx]::notNull($pValue, $this.normalColour) }

    hidden [FortahColour] $selectedColour = [FortahColours]::selectedColour
    [FortahColour] getSelectedColour() { return $this.selectedColour }
    [void] setSelectedColour([FortahColour] $pValue) { $this.selectedColour = [FortahColourEx]::notNull($pValue, $this.selectedColour) }

    hidden [FortahColour] $inactiveColour = [FortahColours]::inactiveColour
    [FortahColour] getInactiveColour() { return $this.inactiveColour }
    [void] setInactiveColour([FortahColour] $pValue) { $this.inactiveColour = [FortahColourEx]::notNull($pValue, $this.inactiveColour) }    

    StateColours() {
    }
    
    StateColours([FortahColour] $pNormalColour, [FortahColour] $pSelectedColour, [FortahColour] $pInactiveColour) {
        $this.setNormalColour($pNormalColour)
        $this.setSelectedColour($pSelectedColour)
        $this.setInactiveColour($pInactiveColour)
    }

    [string] getColour([FortahState] $pState) {
        [FortahColour] $colour = $this.normalColour
        switch ($pState) {
            ([FortahState]::selected) {
                $colour = $this.selectedColour
            }
            ([FortahState]::inactive) {
                $colour = $this.inactiveColour
            }
        }
        return $colour
    }    
    
}

class FortahCharacter {

    static [char] $horizontal = [char]0x2500;
    static [char] $vertical = [char]0x2502;
    static [char] $topLeft = [char]0x250C;
    static [char] $topRight = [char]0x2510;
    static [char] $bottomLeft = [char]0x2514;
    static [char] $bottomRight = [char]0x2518;
    static [char] $leftJoin = [char]0x251C;
    static [char] $rightJoin = [char]0x2524;
    static [char] $topJoin = [char]0x252C;
    static [char] $bottomJoin = [char]0x2534;
    static [char] $on = [char]0x25A0;
    static [char] $off = [char]0x25A1;

}

class FortahConsole {

    static [void] clearScreen() {
        Clear-Host
    }

    static [void] newLine() {
        Write-Host
    }

    static [void] write([Object] $pValue) {
        [FortahConsole]::write($pValue, [FortahColour]::null)
    }

    static [void] writeLine([Object] $pValue) {
        [FortahConsole]::writeLine($pValue, [FortahColour]::null)
    }

    static [void] write([Object] $pValue, [FortahColour] $pColour) {
        [string] $text = [FortahPowerShell]::toString($pValue)
        if ([FortahColourEx]::isNotNull($pColour)) {
            Write-Host ($text) -ForegroundColor ([FortahColourEx]::toSystemConsoleColor($pColour)) -NoNewline
        } else {
            Write-Host ($text) -NoNewline
        }
    }

    static [void] writeLine([Object] $pValue, [FortahColour] $pColour) {
        [FortahConsole]::write($pValue, $pColour)
        [FortahConsole]::newLine()
    }

    static [string] readLine([string] $pPrompt) {
        return [FortahConsole]::readLine($pPrompt, [FortahColour]::null)
    }

    static [string] readLine([string] $pPrompt, [FortahColour] $pColour) {
        [FortahConsole]::write($pPrompt, $pColour)
        return Read-Host
    }    
    
}

class FortahVersion {

    hidden [int] $major = 0
    [int] getMajor() { return $this.major }
    [void] setMajor([int] $pValue) { $this.major = $pValue }

    hidden [int] $minor = 0
    [int] getMinor() { return $this.minor }
    [void] setMinor([int] $pValue) { $this.minor = $pValue }

    hidden [int] $hotfix = 0
    [int] getHotfix() { return $this.hotfix }
    [void] setHotfix([int] $pValue) { $this.hotfix = $pValue }

    hidden [int] $build = 0
    [int] getBuild() { return $this.build }
    [void] setBuild([int] $pValue) { $this.build = $pValue }

    FortahVersion() {
    }

    FortahVersion([int] $pMajor, [int] $pMinor) {
        $this.FortahVersionEx($pMajor, $pMinor)
    }

    hidden [void] FortahVersionEx([int] $pMajor, [int] $pMinor) {
        $this.setMajor($pMajor)
        $this.setMinor($pMinor)
    }

    FortahVersion([int] $pMajor, [int] $pMinor, [int] $pHotfix) {
        $this.FortahVersionEx($pMajor, $pMinor, $pHotfix)
    }

    hidden [void] FortahVersionEx([int] $pMajor, [int] $pMinor, [int] $pHotfix) {
        $this.FortahVersionEx($pMajor, $pMinor)
        $this.setHotfix($pHotfix)
    }

    FortahVersion([int] $pMajor, [int] $pMinor, [int] $pHotfix, [int] $pBuild) {
        $this.FortahVersionEx($pMajor, $pMinor, $pHotfix, $pBuild)
    }

    hidden [void] FortahVersionEx([int] $pMajor, [int] $pMinor, [int] $pHotfix, [int] $pBuild) {
        $this.FortahVersionEx($pMajor, $pMinor, $pHotfix)
        $this.setBuild($pBuild)
    }

    [bool] isEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -eq 0);
    }

    [bool] isNotEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -ne 0);
    }

    [bool] isLower([Version] $pVersion) {
        return ($this.compare($pVersion) -lt 0);
    }

    [bool] isLowerOrEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -le 0);
    }

    [bool] isGreater([Version] $pVersion) {
        return ($this.compare($pVersion) -gt 0);
    }

    [bool] isGreaterOrEqual([Version] $pVersion) {
        return ($this.compare($pVersion) -ge 0);
    }

    [int] compare([Version] $pVersion) {
        [int] $result = [FortahInt]::compare($this.major, $pVersion.getMajor());
        if ($result -eq 0) {
            $result = [FortahInt]::compare($this.minor, $pVersion.getMinor());
        }
        if ($result -eq 0) {
            $result = [FortahInt]::compare($this.hotfix, $pVersion.getHotfix());
        }       
        if ($result -eq 0) {
            $result = [FortahInt]::compare($this.build, $pVersion.getBuild());
        }
        return $result;
    }

    [string] toString() {
        return "$($this.major).$($this.minor).$($this.hotfix).$($this.build)"
    }

    static [Version] parse([string] $pText) {
        [Version] $version = [Version]::new()
        [List[string]] $textParts = $pText.Split('.')
        if ($textParts.Length -eq 4) {
            $version.setMajor([int]::parse($textParts[0]))
            $version.setMinor([int]::parse($textParts[1]))
            $version.setHotfix([int]::parse($textParts[2]))
            $version.setBuild([int]::parse($textParts[3]))
        } else {
            throw "Text '$pText' does not represent a version."
        }
        return $version
    }
}

class FortahSub {

    hidden [Object] $super = $null
    [Object] getSuper() { return $this.super }
    [void] setSuper([Object] $pValue) { $this.super = $pValue }

    Sub() {
    }

    Sub([Object] $pSuper) {
        $this.setSuper($pSuper)
    }
    
}

class FortahConfigBase {

    static [string] $defaultFileName = 'config.json'

    FortahConfigBase() {
    }

    [void] read() {
        $this.read('')
    }

    [void] read([string] $pFilePath) {
        [string] $filePath = [FortahConfigLocator]::findFilePath($pFilePath)
        [PSCustomObject] $json = (Get-Content -Path $filePath -Raw | ConvertFrom-Json)
        $this.readInt($json)
    }

    [void] readInt([PSCustomObject] $pJson) {
    }

}

class FortahConfigLocator {

    static [string] findFilePath([string] $pFilePath) {
        [string] $filePath = ''
        if ([FortahConfigLocator]::isFilePathAFileName($pFilePath)) {
            $filePath = [Path]::Combine($PSScriptRoot, $pFilePath)
        } else {
            $filePath = $pFilePath
        }
        if (-not([File]::Exists($filePath))) {
            [string] $errorMessage = "Cannot find config file '$filePath'." 
            throw $errorMessage
        }
        return $filePath
    }

    hidden static [bool] isFilePathAFileName([string] $pFilePath) {
        return ([Path]::GetFileName($pFilePath) -eq $pFilePath)
    }

}

class FortahLogColours {

    hidden [FortahColour] $titleColour = [FortahColour]::titleColour
    [FortahColour] getTitleColour() { return $this.titleColour }
    [void] setTitleColour([FortahColour] $pValue) { $this.titleColour = [FortahPowerShell]::notNull($pValue, $this.titleColour) }

    hidden [FortahColour] $informationColour = [FortahColours]::textColour
    [FortahColour] getInformationColour() { return $this.informationColour }
    [void] setInformationColour([FortahColour] $pValue) { $this.informationColour = [FortahPowerShell]::notNull($pValue, $this.informationColour) }
    
    hidden [FortahColour] $warningColour = [FortahColours]::warningColour
    [FortahColour] getWarningColour() { return $this.warningColour }
    [void] setWarningColour([FortahColour] $pValue) { $this.warningColour = [FortahPowerShell]::notNull($pValue, $this.warningColour) }

    hidden [FortahColour] $errorColour = [FortahColour]::errorColour
    [FortahColour] getErrorColour() { return $this.errorColour }
    [void] setErrorColour([FortahColour] $pValue) { $this.errorColour = [FortahPowerShell]::notNull($pValue, $this.errorColour) }

    LogColours() {
    }

    LogColours([FortahColour] $pTitleColour, [FortahColour] $pInformationColour, [FortahColour] $pWarningColour, [FortahColour] $pErrorColour) {
        $this.setTitleColour($pTItleColour)
        $this.setInformationColour($pInformationColour)
        $this.setWarningColour($pWarningColour)
        $this.setErrorColour($pErrorColour)
    }

}

enum FortahLogItemType {
    null = 0
    message = 1
    log = 2
}

class FortahLogItem {

    hidden [FortahLogItemType] $type = [FortahLogItemType]::null
    [FortahLogItemType] getType() { return $this.type }
    [void] setType([FortahLogItemType] $pValue) { $this.type = $pValue }

    FortahLogItem([FortahLogItemType] $pType) {
        $this.setType($pType)
    }

}

enum FortahLogMessageType {
    Null = 0
    Information = 1
    Warning = 2
    Error = 3
}

class FortahLogMessage : FortahLogItem {

    hidden [FortahLogMessageType] $messageType = [FortahLogMessageType]::null
    [FortahLogMessageType] getMessageType() { return $this.messageType }
    [void] setMessageType([FortahLogMessageType] $pValue) { $this.messageType = $pValue }

    hidden [string] $text = ''
    [string] getText() { return $this.text }
    [void] setText([string] $pValue) { $this.text = $pValue }

    FortahLogMessage() : base([FortahLogItemType]::message) {
    }

    FortahLogMessage([string] $pText) : base([FortahLogItemType]::message) {
        $this.FortahLogMessageEx([FortahLogMessageType]::information, $pText)
    }
    
    FortahLogMessage([FortahLogMessageType] $pMessageType, [string] $pText) : base([FortahLogItemType]::message) {
        $this.FortahLogMessageEx($pMessageType, $pText)
    }
    
    hidden [void] FortahLogMessageEx([FortahLogMessageType] $pMessageType, [string] $pText) {
        $this.setMessageType($pMessageType)
        $this.setText($pText)
    }

    [bool] isWarning() {
        return ($this.messageType -eq [FortahLogMessageType]::warning)
    }

    [bool] isError() {
        return ($this.messageType -eq [FortahLogMessageType]::error)
    }

    [bool] isErrorOrWarning() {
        return (($this.messageType -eq [FortahLogMessageType]::error) -or ($this.messageType -eq [FortahLogMessageType]::warning))
    }

    [FortahColour] getColour([FortahLogColours] $pColours) {
        [FortahColour] $colour = [FortahColours]::textColour
        switch ($this.messageType) {
            ([FortahLogMessageType]::information) {
                $colour = $pColours.getInformationColour()
            }
            ([FortahLogMessageType]::warning) {
                $colour = $pColours.getWarningColour()
            }
            ([FortahLogMessageType]::error) {
                $colour = $pColours.getErrorColour()
            }
        }
        return $colour
    }

    [string] toString() {
        [string] $string = ''
        switch ($this.messageType) {
            ([FortahLogMessageType]::warning) {
                $string = 'WARNING'
            }
            ([FortahLogMessageType]::warning) {
                $string = 'ERROR'
            }
        }
        if ($string -ne '') {
            $string += ': '
        }
        $string += $this.text
        return $string
    }
}

class FortahLogPropertyMessage : FortahLogMessage {

    FortahLogPropertyMessage([string] $pName, [Object] $pValue) : base([FortahLogPropertyMessage]::createText($pName, $pValue)) {
    }

    FortahLogPropertyMessage([FortahLogMessageType] $pType, [string] $pName, [Object] $pValue) : base($pType, [FortahLogPropertyMessage]::createText($pName, $pValue)) {
    }

    hidden static [string] createText([string] $pName, [Object] $pValue) {
        return "$($pName): $([FortahPowerShell]::toString($pValue))"
    }
    
}

class FortahLog : FortahLogItem {

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }

    hidden [List[FortahLogItem]] $items = [List[FortahLogItem]]::new()
    [List[FortahLogItem]] getItems() { return $this.items }
    [void] setItems([List[FortahLogItem]] $pValue) { $this.items = $pValue }

    FortahLog() : base([FortahLogItemType]::log) {
    }

    FortahLog([string] $pName) : base([FortahLogItemType]::log) {
        $this.setName($pName)
    }

    FortahLog([List[FortahLogItem]] $pItems) : base([FortahLogItemType]::log) {
        $this.add($pItems)
    }

    FortahLog([string] $pName, [List[FortahLogItem]] $pItems) : base([FortahLogItemType]::log) {
        $this.setName($pName)
        $this.add($pItems)
    }

    FortahLog([Hashtable] $pHash) : base([FortahLogItemType]::log) {
        $this.add($pHash)
    }

    FortahLog([string] $pName, [Hashtable] $pHash) : base([FortahLogItemType]::log) {
        $this.setName($pName)
        $this.add($pHash)
    }

    [void] addInformation([string] $pText) {
        $this.add([FortahLogMessageType]::information, $pText)
    }
    
    [void] addInformation([List[string]] $pTexts) {
        $this.add([FortahLogMessageType]::information, $pTexts)
    }

    [void] addWarning([string] $pText) {
        $this.add([FortahLogMessageType]::warning, $pText)
    }

    [void] addError([string] $pText) {
        $this.add([FortahLogMessageType]::error, $pText)
    }

    [void] add([FortahLogMessageType] $pType, [string] $pText) {
        $this.items.Add([FortahLogPropertyMessage]::new($pType, $pText))
    }

    [void] add([FortahLogMessageType] $pType, [List[string]] $pTexts) {
        [string] $text = ''
        foreach ($text in $pTexts) {
            $this.add($pType, $text)
        }
    }

    [void] add([string] $pName, [Object] $pValue) {
        $this.items.Add([FortahLogPropertyMessage]::new([FortahLogMessageType]::information, $pName, $pValue))
    }

    [void] add([FortahLogItem] $pItem) {
        $this.items.add($pItem)
    }

    [void] add([List[FortahLogItem]] $pItems) {
        [FortahLogItem] $item = $null
        foreach ($item in $pItems) {
            $this.items.add($item)
        }
    }

    [void] add([Hashtable] $pHash) {
        [string] $key = ''
        foreach ($key in $pHash.Keys) {
            $this.add($key, $pHash[$key])
        }
    }

    [void] testString([string] $pValue, [string] $pName) {
        $this.testStringEx($pValue, "$pName cannot be empty")
    }

    [void] testStringEx([string] $pValue, [string] $pText) {
        if ($pValue -eq '') {
            $this.addError($pText)
        }
    }

    [void] testInt([int] $pValue, [string] $pName) {
        $this.testIntEx($pValue, "$pName cannot be empty")
    }

    [void] testIntEx([int] $pValue, [string] $pText) {
        if ($pValue -le 0) {
            $this.addError($pText)
        }
    }

    [bool] hasWarnings() {
        [bool] $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::message) -and ($_.isWarning()))}))
        if (-not ($result)) {
            $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::log) -and ($_.hasWarnings()))}))
        }
        return $result
    }

    [bool] hasErrors() {
        [bool] $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::message) -and ($_.isError()))}))
        if (-not ($result)) {
            $result = [FortahPowerShell]::isNotNull(($this.items | Where-Object {(($_.getType() -eq [FortahLogItemType]::log) -and ($_.hasErrors()))}))
        }
        return $result
    }

    [bool] hasErrorsOrWarnings() {
        [Bool] $result = $this.hasErrors()
        if (-not ($result)) {
            $result = $this.hasWarnings()
        }
        return $result
    }

    [void] throwExceptionIfHasErrors() {
        if ($this.hasErrors()) {
            throw $this.toString()
        }
    }

    [string] toString() {
        [string] $string = ''
        [FortahLogItem] $item = $null
        if ($this.name -ne '') {
            $string += "$($this.name): {"
        }
        foreach ($item in $this.items) {
            if ($string.Length -gt 0) {
                $string += " "
            }
            $string += $item.toString()
        }
        if ($this.name -ne '') {
            $string += '}'
        }
        return $string
    }
}

class FortahLogRenderer {

    hidden [FortahLogColours] $colours = [FortahLogColours]::new()
    [FortahLogColours] getColours() { return $this.colours }
    [void] setColours([FortahLogColours] $pValue) { $this.colours = $pValue }

    hidden [int] $tabSize = 2
    [int] getTabSize() { return $this.tabSize }
    [void] setTabSize([int] $pValue) { $this.tabSize = $pValue }

    FortahLogRenderer() {        
    }

    FortahLogRenderer([FortahLogColours] $pColours) {
        $this.FortahLogRendererEx($pColours)
    }

    hidden [void] FortahLogRendererEx([FortahLogColours] $pColours) {
        $this.setColours($pColours)
    }

    FortahLogRenderer([FortahLogColours] $pColours, [int] $pTabSize) {
        $this.FortahLogRendererEx($pColours, $pTabSize)
    }

    hidden [void] FortahLogRendererEx([FortahLogColours] $pColours, [int] $pTabSize) {
        $this.FortahLogRendererEx($pColours)
        $this.tabSize = $pTabSize
    }    

    [void] render([FortahLog] $pLog) {
        $this.render($pLog, 0)
    }

    [void] render([FortahLog] $pLog, [int] $pIndentation) {
        [int] $itemsIndentation = 0
        if ($pLog.getName() -ne '') {
            [string] $text = [FortahString]::indent($pLog.getName(), $pIndentation * $this.tabSize)
            [FortahConsole]::writeLine($text, $this.colours.getTitleColour())
            $itemsIndentation = 1
        }
        [FortahLogItem] $item = $null
        foreach ($item in $pLog.items) {
            switch ($item.getType()) {
                ([FortahLogItemType]::message) {
                    [string] $text = [FortahString]::indent($item.toString(), ($pIndentation + $itemsIndentation) * $this.tabSize)
                    [FortahConsole]::writeLine($text, $item.getColour($this.colours))
                }
                ([FortahLogItemType]::log) {
                    $this.render($item, $pIndentation + 1)
                }
            }
        }
    }
    
}

class FortahManifest {

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }
    
    hidden [string] $description = ''
    [string] getDescription() { return $this.description }
    [void] setDescription([string] $pValue) { $this.description = $pValue }

    hidden [FortahVersion] $version = [FortahVersion]::new()
    [FortahVersion] getVersion() { return $this.version }
    [void] setVersion([FortahVersion] $pValue) { $this.version = $pValue }

    hidden [string] $company = ''
    [string] getCompany() { return $this.company }
    [void] setCompany([string] $pValue) { $this.company = $pValue }

    hidden [string] $author = ''
    [string] getAuthor() { return $this.author }
    [void] setAuthor([string] $pValue) { $this.author = $pValue }

    hidden [string] $date = ''
    [string] getDate() { return $this.date }
    [void] setDate([string] $pValue) { $this.date = $pValue }

    FortahManifest([string] $pName, [string] $pDescription, [FortahVersion] $pVersion, [string] $pCompany, [string] $pAuthor, [string] $pDate) {
        $this.setName($pName)
        $this.setDescription($pDescription)
        $this.setVersion($pVersion)
        $this.setCompany($pCompany)
        $this.setAuthor($pAuthor)
        $this.setDate($pDate)
    }

    [string] toString() {
        return "$($this.company) - $($this.name) - $($this.version.toString()) - $($this.author) - $($this.date)"
    }

    [string] toTitleLine1() {
        return "$($this.company) - $($this.name) - $($this.version.toString())"
    }

    [string] toTitleLine2() {
        return "$($this.author) - $($this.date)"
    }
}

class FortahCommand {

    hidden [string] $code = ''
    [string] getCode() { return $this.code }

    hidden [string] $key = ''
    [string] getKey() { return $this.key }

    hidden [string] $text = ''
    [string] getText() { return $this.text }

    FortahCommand() {
    }

    FortahCommand([string] $pCode, [string] $pKey) {
        $this.FortahCommandEx($pCode, $pKey, $pKey)
    }

    FortahCommand([string] $pCode, [string] $pKey, [string] $pText) {
        $this.FortahCommandEx($pCode, $pKey, $pText)
    }    

    hidden [void] FortahCommandEx([string] $pCode, [string] $pKey, [string] $pText) {
        $this.code = $pCode
        $this.key = $pKey
        $this.text = $pText
    }

    [string] toString() {
        return $this.text
    }
}

class FortahHandlingResult {

    hidden [bool] $handled = $false
    [bool] isHandled() { return $this.handled }
    [void] setHandled([bool] $pValue) { $this.handled = $pValue }

    hidden [bool] $followedByEnter = $false
    [bool] isFollowedByEnter() { return $this.followedByEnter }
    [void] setFollowedByEnter([bool] $pValue) { $this.followedByEnter = $pValue }

    FortahHandlingResult() {
    }

    FortahHandlingResult([bool] $pHandled) {
        $this.setHandled($pHandled)
    }

    FortahHandlingResult([bool] $pHandled, [bool] $pFollowedByEnter) {
        $this.setHandled($pHandled)
        $this.setFollowedByEnter($pFollowedByEnter)
    }

    [void] setTrue() {
        $this.setHandled($true)
        $this.setFollowedByEnter($true)
    }

    [void] setHandledTrue() {
        $this.setHandled($true)
    }

}

class FortahHandlingOptions {

    hidden [string] $initialMessage = ''
    [string] getInitialMessage() { return $this.initialMessage }
    [void] setInitialMessage([string] $pValue) { $this.initialMessage = $pValue }
    
    hidden [string] $finalMessage = ''
    [string] getFinalMessage() { return $this.finalMessage }
    [void] setFinalMessage([string] $pValue) { $this.finalMessage = $pValue }

    hidden [bool] $onlyFirstResult = $false
    [bool] isOnlyFirstResult() { return $this.onlyFirstResult }
    [void] setOnlyFirstResult([bool] $pValue) { $this.onlyFirstResult = $pValue }
    
    FortahHandlingOptions([string] $pInitialMessage, [string] $pFinalMessage) {
        $this.FortahHandlingOptionsEx($pInitialMessage, $pFinalMessage)
    }

    hidden [void] FortahHandlingOptionsEx([string] $pInitialMessage, [string] $pFinalMessage) {
        $this.setInitialMessage($pInitialMessage)
        $this.setFinalMessage($pFinalMessage)
    }

    FortahHandlingOptions([string] $pInitialMessage, [string] $pFinalMessage, [bool] $pOnlyFirstResult) {
        $this.FortahHandlingOptionsEx($pInitialMessage, $pFinalMessage)
        $this.setOnlyFirstResult($pOnlyFirstResult)
    }

    FortahHandlingOptions([string] $pPrefix) {
        $this.FortahHandlingOptionsEx($pPrefix, 'in progress...', 'completed.')
    }

    FortahHandlingOptions([string] $pPrefix, [bool] $pOnlyFirstResult) {
        $this.FortahHandlingOptionsEx($pPrefix, 'in progress...', 'completed.')
        $this.setOnlyFirstResult($pOnlyFirstResult)
    }

    FortahHandlingOptions([string] $pPrefix, [string] $pInitialMessageSuffix, [string] $pFinalMessageSuffix) {
        $this.FortahHandlingOptionsEx($pPrefix, $pInitialMessageSuffix, $pFinalMessageSuffix)
    }

    FortahHandlingOptions([string] $pPrefix, [string] $pInitialMessageSuffix, [string] $pFinalMessageSuffix, [bool] $pOnlyFirstResult) {
        $this.FortahHandlingOptionsEx($pPrefix, $pInitialMessageSuffix, $pFinalMessageSuffix)
        $this.setOnlyFirstResult($pOnlyFirstResult)
    }

    hidden [void] FortahHandlingOptionsEx([string] $pPrefix, [string] $pInitialMessageSuffix, [string] $pFinalMessageSuffix) {
        if (($pPrefix -ne '') -and ($pInitialMessageSuffix -ne '')) {
            $this.setInitialMessage($pPrefix + ' ' + $pInitialMessageSuffix)
        }
        if (($pPrefix -ne '') -and ($pFinalMessageSuffix -ne '')) {
            $this.setFinalMessage($pPrefix + ' ' + $pFinalMessageSuffix)
        }
    }
}

class FortahHandler : FortahSub {
    
    hidden [Object] getLoop() { return $this.super() }
    
    FortahHandler([Object] $pSuper) : base($pSuper) {
    }

    [void] handle([FortahCommand] $pCommand) {
    }

    hidden [Object] run([ScriptBlock] $pCode, [FortahHandlingOptions] $pOptions) {
        return $this.run($pCode, $null, $pOptions)
    }

    hidden [Object] run([ScriptBlock] $pCode, [Object] $pArguments, [FortahHandlingOptions] $pOptions) {
        [Object] $result = $null

        $this.renderMessage($pOptions.getInitialMessage(), $false, $false)
        
        [Collection`1[PSObject]] $invokeResult = $null

        if ([FortahPowerShell]::isNotNull($pArguments)) {
            $invokeResult = $pCode.Invoke($this, $pArguments)
        } else {
            $invokeResult = $pCode.Invoke($this)
        }

        if ([FortahPowerShell]::isNotNull($invokeResult)) {
            if ($pOptions.isOnlyFirstResult()) {
                $result = $invokeResult[0]
            } else {
                $result = $invokeResult
            }
        }

        $this.renderMessage($pOptions.getFinalMessage(), $false, $false)

        return $result
    }

    hidden [void] renderMessage([string] $pMessage, [bool] $pNewLineBefore, [bool] $pNewLineAfter) {
        if ($pNewLineBefore) {
            [FortahConsole]::newLine()
        }
        if ([FortahPowerShell]::isStringNotEmpty($pMessage)) {
            [FortahConsole]::writeLine($pMessage, [FortahColours]::textColour)
        }
        if ($pNewLineAfter) {
            [FortahConsole]::newLine()
        }
    }

    hidden [void] pretend([string] $pDescription, [string] $pClass, [string] $pMethod) {
        [FortahConsole]::writeLine("$pDescription --> [$pClass]::$pMethod()", [FortahColours]::debugColour)
    }
    
}

enum FortahLoopState {
    null = 0
    userInput = 1
    handling = 2
}

class FortahLoop {
    hidden [FortahWindow] $window = $null
    [FortahWindow] getWindow() { return $this.window }

    hidden [FortahHandler] $handler = $null
    [FortahHandler] getHandler() { return $this.handler }

    hidden [bool] $running = $false
    [bool] isRunning() { return $this.running }

    FortahLoop() {        
    }

    [FortahWindow] createWindow() {
        return $null
    }

    [FortahHandler] createHandler() {
        return $null
    }

    [void] run() {
        $this.start()
        while ($this.running) {
            [string] $userInput = $this.render([FortahLoopState]::userInput)
            [FortahCommand] $command = $this.findCommand($userInput)
            $this.handle($command)
        }
        $this.finish()
    }
    
    hidden [void] start() {
        $this.window = $this.createWindow()
        $this.handler = $this.createHandler()
        $this.running = (($null -ne $this.window) -and ($null -ne $this.handler))
    }

    hidden [string] render([FortahLoopState] $pLoopState) {
        $this.updateWindow($pLoopState)
        [FortahConsole]::clearScreen() 
        $this.window.render()
        return $this.window.getUserInput()
    }

    hidden [void] updateWindow([FortahLoopState] $pLoopState) {        
    }

    hidden [FortahCommand] findCommand([string] $pKey) {
        [FortahCommand] $command = $this.window.findCommand($pKey)
        if ($null -eq $command) {
            [FortahConsole]::readLine("Unknown command: ""$($pKey)"". Please, press ""Enter"" and try again: ", [FortahColours]::errorColour)
        }
        return $command
    }

    hidden [void] handle([FortahCommand] $pCommand) {
        if ($null -ne $pCommand) {
            $this.render([FortahLoopState]::handling)
            $this.handler.handle($pCommand)
        }
    }

    [void] stop() {
        $this.running = $false
    }

    hidden [void] finish() {
    }

    [void] exit() {
        $this.running = $false
    }    
}

class FortahApplicationBase {

    hidden [FortahManifest] $manifest = $null
    [FortahManifest] getManifest() { return $this.manifest }

    hidden [FortahConfigBase] $config = $null
    [FortahConfigBase] getConfig() { return $this.config }
    
    hidden [FortahLoop] $loop = $null
    [FortahLoop] getLoop() { return $this.loop }

    FortahApplicationBase([FortahManifest] $pManifest) {
        $this.manifest = $pManifest
    }

    FortahApplicationBase([FortahManifest] $pManifest, [bool] $pReadConfig) {
        $this.manifest = $pManifest
        if ($pReadConfig) {
            $this.readConfig('')
        }
    }

    FortahApplicationBase([FortahManifest] $pManifest, [string] $pConfigFilePath) {
        $this.manifest = $pManifest
        $this.readConfig($pConfigFilePath)
    }

    FortahApplicationBase([FortahManifest] $pManifest, [FortahConfigBase] $pConfig) {
        $this.manifest = $pManifest
        $this.config = $pConfig
    }

    hidden [void] readConfig([string] $pConfigFilePath) {
        $this.config = $this.createConfig() 
        if ($null -ne $this.config) {
            $this.config.read($pConfigFilePath)
        }
    }

    [FortahConfigBase] createConfig() {
        return [FortahConfigBase]::new()
    }

    [FortahLoop] createLoop() {
        return $null
    }

    [void] run() {
        $this.start()
        if ($null -ne $this.loop) {
            $this.loop.run()
        }
        $this.finish()
    }

    [void] start() {
        $this.loop = $this.createLoop()
    }

    [void] finish() {
        [FortahConsole]::newLine()
        [FortahConsole]::writeLine('Good bye!', [FortahColours]::selectedColour)
        [FortahConsole]::newLine()
    }

}

class FortahControl {

    hidden [string] $name = ''
    [string] getName() { return $this.name }
    [void] setName([string] $pValue) { $this.name = $pValue }

    hidden [int] $width = 0
    [int] getWidth() { return $this.width }
    [void] setWidth([int] $pValue) { $this.width = $pValue }

    hidden [FortahColour] $colour = [FortahColours]::textColour
    [FortahColour] getColour() { return $this.colour }
    [void] setColour([FortahColour] $pValue) { $this.colour = $pValue }

    hidden [bool] $hidden = $false
    [bool] isHidden() { return $this.hidden }
    [void] setHidden([bool] $pValue) { $this.hidden = $pValue }

    [string] getUserInput() { return $null }

    FortahControl() {
    }

    FortahControl([string] $pName) {
        $this.name = $pName
    }

    FortahControl([int] $pWidth) {
        $this.width = $pWidth
    }

    FortahControl([string] $pName, [int] $pWidth) {
        $this.name = $pName
        $this.width = $pWidth
    }
    
    FortahControl([FortahColour] $pColour) {
        $this.colour = $pColour
    }

    FortahControl([string] $pName, [FortahColour] $pColour) {
        $this.name = $pName
        $this.colour = $pColour
    }
    
    FortahControl([int] $pWidth, [FortahColour] $pColour) {
        $this.width = $pWidth
        $this.colour = $pColour
    }

    FortahControl([string] $pName, [int] $pWidth, [FortahColour] $pColour) {
        $this.name = $pName
        $this.width = $pWidth
        $this.colour = $pColour
    }

    [int] calculateWidth() {
        return $this.width
    }

    [List[FortahControl]] toRenderables() {
        return $null
    }

    [void] appendToRenderables([List[FortahControl]] $pRenderables) {
        [List[FortahControl]] $thisRenderables = $this.toRenderables()
        if ($null -ne $thisRenderables) {
            $pRenderables.AddRange($thisRenderables)
        }
    }

    [void] render() {        
    }

    [FortahControl] findControl([string] $pName) {
        if ($this.name -eq $pName) {
            return $this
        } else {
            return $null
        }
    }

    [FortahCommand] findCommand([string] $pKey) {
        return $null
    }    
    
}

class FortahNewLine : FortahControl {

    hidden [int] $noOf = 1
    [int] getNoOf() { return $this.noOf }
    [void] setNoOf([int] $pValue) { $this.noOf = $pValue }

    FortahNewLine() : base() {        
    }
    
    FortahNewLine([string] $pName) : base($pName) {
    }

    FortahNewLine([int] $pNoOf) : base() {
        $this.noOf = $pNoOf        
    }

    FortahNewLine([string] $pName, [int] $pNoOf) : base($pName) {
        $this.noOf = $pNoOf        
    }

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }

    [void] render() {
        for ([int] $index = 0; $index -lt $this.noOf; $index++) {
            [FortahConsole]::newLine()
        }
    }
    
}

class FortahSeparator : FortahControl {
    
    FortahSeparator() : base() {
    }
    
    FortahSeparator([string] $pName): base($pName) {
    }

    FortahSeparator([int] $pWidth) : base($pWidth) {        
    } 

    FortahSeparator([string] $pName, [int] $pWidth) : base($pName, $pWidth) {
    } 

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }    
    
}

class FortahSpace : FortahControl {

    FortahSpace() : base() { 
    }

    FortahSpace([string] $pName) : base($pName) { 
    }

    FortahSpace([int] $pWidth) : base($pWidth) { 
    }

    FortahSpace([string] $pName, [int] $pWidth) : base($pName, $pWidth) { 
    }

    [void] render() {
        if ($this.width -gt 0) {
            [FortahConsole]::write(' ' * $this.width)
        }
    }

}

class FortahLabel : FortahControl {

    hidden [string] $text = ''
    [string] getText() { return $this.text }
    [void] setText([string] $pValue) { $this.text = $pValue }
    
    FortahLabel([string] $pText) : base() {
        $this.text = $pText
    }

    FortahLabel([string] $pName, [string] $pText) : base($pName) {
        $this.text = $pText
    }

    FortahLabel([string] $pText, [int] $pWidth) : base($pWidth) {
        $this.text = $pText
    }

    FortahLabel([string] $pName, [string] $pText, [int] $pWidth) : base($pName, $pWidth) {
        $this.text = $pText
    }

    FortahLabel([string] $pText, [FortahColour] $pColour) : base($pColour) {
        $this.text = $pText
    }
    
    FortahLabel([string] $pName, [string] $pText, [FortahColour] $pColour) : base($pName, $pColour) {
        $this.text = $pText
    }

    FortahLabel([string] $pText, [int] $pWidth, [FortahColour] $pColour) : base($pWidth, $pColour) {
        $this.text = $pText
    }

    FortahLabel([string] $pName, [string] $pText, [int] $pWidth, [FortahColour] $pColour) : base($pName, $pWidth, $pColour) {
        $this.text = $pText
    }

    [int] calculateWidth() {
        [int] $width = $this.width
        if ($this.text.Length -gt $width) {
            $width = $this.text.Length
        }
        return $width
    }

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }

    [void] render() {
        [string] $textToRender = $this.text
        if ($this.width -gt 0) {
            if ($this.width -gt $textToRender.Length) {
                $textToRender += ' ' * ($this.width - $textToRender.Length)
            } else {
                if ($this.width -lt $textToRender.Length) {
                    $textToRender = $textToRender.Substring(0, $this.width)
                }
            }
        }
        [FortahConsole]::write($textToRender, $this.colour)
    }
}

class FortahContainer : FortahControl {

    hidden [List[FortahControl]] $controls = [List[FortahControl]]::new()
    [List[FortahControl]] getControls() { return $this.controls }
    [void] setControls([List[FortahControl]] $pValue) { $this.controls = $pValue }

    FortahContainer() : base() {        
    }

    FortahContainer([string] $pName) : base($pName) {
    }

    FortahContainer([List[FortahControl]] $pControls) : base() {
        $this.controls = $pControls
    }
    
    FortahContainer([string] $pName, [List[FortahControl]] $pControls) : base($pName) {
        $this.controls = $pControls
    }

    FortahContainer([int] $pWidth, [List[FortahControl]] $pControls) : base($pWidth) {
        $this.controls = $pControls
    }

    FortahContainer([string] $pName, [int] $pWidth, [List[FortahControl]] $pControls) : base($pName, $pWidth) {
        $this.controls = $pControls
    }

    FortahContainer([FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pColour) {
        $this.controls = $pControls
    }

    FortahContainer([string] $pName, [FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pName, $pColour) {
        $this.controls = $pControls
    }

    FortahContainer([int] $pWidth, [FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pWidth, $pColour) {
        $this.controls = $pControls
    }

    FortahContainer([string] $pName, [int] $pWidth, [FortahColour] $pColour, [List[FortahControl]] $pControls) : base($pName, $pWidth, $pColour) {
        $this.controls = $pControls
    }

    [void] addControl([FortahControl] $pControl) {
        $this.controls.Add($pControl)
    }

    [void] addControls([List[FortahControl]] $pControls) {
        $this.controls.AddRange($pControls)
    }

    [List[FortahControl]] toRenderables() {
        [List[FortahControl]] $renderables = [List[FortahControl]]::new()
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ( -not ($control.isHidden())) {
                [List[FortahControl]] $controlRenderables = $control.toRenderables()
                if ($null -ne $controlRenderables) {
                    $renderables.AddRange($controlRenderables)
                }
            }
        }
        return $renderables
    }

    [FortahControl] findControl([string] $pName) {
        [FortahControl] $controlFound = $null
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            $controlFound = $control.findControl($pName)
            if ($null -ne $controlFound) {
                break
            }
        }
        return $controlFound
    }

    [FortahCommand] findCommand([string] $pKey) {
        [FortahCommand] $command = $null
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            $command = $control.findCommand($pKey)
            if ($null -ne $command) {
                break
            }
        }
        return $command
    }
    
}

class FortahShelf : FortahContainer {

    hidden [int] $space = 1
    [int] getSpace() { return $this.space }
    [void] setSpace([int] $pValue) { $this.space = $pValue }    

    FortahShelf() : base() {
    }
    
    FortahShelf([string] $pName) : base($pName) {
    }

    FortahShelf([int] $pSpace) : base() {
        $this.space = $pSpace
    }

    FortahShelf([string] $pName, [int] $pSpace) : base($pName) {
        $this.space = $pSpace
    }

    FortahShelf([List[FortahControl]] $pControls) : base($pControls) {
    }

    FortahShelf([List[FortahControl]] $pControls, [int] $pSpace) : base($pControls) {
        $this.space = $pSpace
    }

    [int] calculateWidth() {
        [int] $width = $this.width
        [int] $controlsWidth = 0
        [bool] $first = $true
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ($first) {
                $first = $false
                $controlsWidth += $control.calculateWidth()
            } else {
                if ($control -is [FortahSeparator]) {
                    if ($controlsWidth -gt $width) {
                        $width = $controlsWidth
                    }
                    $controlsWidth = 0
                } else {
                    if ($this.space -gt 0) {
                        $controlsWidth += $this.space
                    }
                    $controlsWidth += $control.calculateWidth()
                }
            }
        }
        if ($controlsWidth -gt $width) {
            $width = $controlsWidth
        }
        return $width
    }

    [List[FortahControl]] toRenderables() {
        [List[FortahControl]] $renderables = [List[FortahControl]]::new()
        [bool] $first = $true
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ($first) {
                $first = $false
                $control.appendToRenderables($renderables)
            } else {
                if ($control -is [FortahSeparator]) {
                    $first = $true
                    $renderables.Add([FortahNewLine]::new())
                } else {
                    if ($this.space -gt 0) {
                        $renderables.Add([FortahLabel]::new(' ', $this.space))
                    }
                    $control.appendToRenderables($renderables)
                }
            }
        }
        return $renderables
    }
    
}

class FortahStack : FortahContainer {

    hidden [int] $space = 1
    [int] getSpace() { return $this.space }
    [void] setSpace([int] $pValue) { $this.space = $pValue }  

    FortahStack() : base() {
    }
    
    FortahStack([string] $pName) : base($pName) {
    }
    
    FortahStack([int] $pSpace) : base() {
        $this.space = $pSpace
    }

    FortahStack([string] $pName, [int] $pSpace) : base($pName) {
        $this.space = $pSpace
    }

    FortahStack([List[FortahControl]] $pControls) : base($pControls) {
    }
    
    FortahStack([string] $pName, [List[FortahControl]] $pControls) : base($pName, $pControls) {
    }

    FortahStack([List[FortahControl]] $pControls, [int] $pSpace) : base($pControls) {
        $this.space = $pSpace
    }

    FortahStack([string] $pName, [List[FortahControl]] $pControls, [int] $pSpace) : base($pName, $pControls) {
        $this.space = $pSpace
    }

    [int] calculateWidth() {
        [int] $width = $this.width
        [int] $controlsWidth = 0
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            [int] $controlWidth = $control.calculateWidth()
            if ($controlWidth -gt $controlsWidth) {
                $controlsWidth = $controlWidth
            }
        }
        if ($controlsWidth -gt $width) {
            $width = $controlsWidth
        }
        return $width
    }

    [List[FortahControl]] toRenderables() {
        [List[FortahControl]] $renderables = [List[FortahControl]]::new()
        [bool] $first = $true
        [bool] $separator = $false
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            if ($first) {
                $first = $false
            } else {
                if ($control -is [FortahSeparator]) {
                    $separator = $true
                } else {
                    if (( -not ($separator)) -and ($this.space -gt 0)) {
                        $renderables.Add([FortahNewLine]::new($this.space))
                    }
                    $separator = $false
                }
            }
            $control.appendToRenderables($renderables)
        }
        return $renderables
    }
    
}

class FortahFrame : FortahStack {

    hidden [List[FortahControl]] $renderables = $null
    hidden [int] $width = 0
    hidden [int] $outerWidth = 0
    hidden [int] $currentWidth = 0

    FortahFrame() : base() {        
    }

    FortahFrame([int] $pWidth) : base() {
        $this.width = $pWidth
    }

    FortahFrame([string] $pName, [int] $pWidth) : base($pName) {
        $this.width = $pWidth
    }

    FortahFrame([List[FortahControl]] $pControls) : base($pControls) {
        $this.colour = [FortahColours]::borderColour
    }

    FortahFrame([string] $pName, [List[FortahControl]] $pControls) : base($pName, $pControls) {
        $this.colour = [FortahColours]::borderColour
    }

    FortahFrame([int] $pWidth, [List[FortahControl]] $pControls) : base($pControls) {
        $this.width = $pWidth
        $this.colour = [FortahColours]::borderColour
    }

    FortahFrame([string] $pName, [int] $pWidth, [List[FortahControl]] $pControls) : base($pName, $pControls) {
        $this.width = $pWidth
        $this.colour = [FortahColours]::borderColour
    }

    [int] calculateWidth() {
        $this.width = ([FortahStack]$this).calculateWidth()
        $this.outerWidth = $this.width + 4
        $this.currentWidth = 0
        return $this.width
    }

    [List[FortahControl]] toRenderables() {
        $this.renderables = [List[FortahControl]]::new()
        $this.calculateWidth()
        $this.addHeader()
        [FortahControl] $control = $null
        foreach ($control in $this.controls) {
            [List[FortahControl]] $controlRenderables = $control.toRenderables()
            [FortahControl] $controlRenderable = $null
            foreach ($controlRenderable in $controlRenderables) {
                if ($controlRenderable -is [FortahNewLine]) {
                    $this.processNewLine([FortahNewLine]$controlRenderable)
                } else {
                    if ($controlRenderable -is [FortahSeparator]) {
                        $this.processSeparator([FortahSeparator]$controlRenderable)
                    } else {
                        $this.processControl($controlRenderable)
                    }
                }
            }
        }
        $this.addFooter()
        return $this.renderables
    }

    hidden [void] addHeader() {
        $this.addTopBorder()
        $this.addNewLine()
        $this.addLeftBorder()
    }

    hidden [void] processNewLine([FortahNewLine] $pNewLine) {
        for ([int] $index = 0; $index -lt $pNewLine.getNoOf(); $index++) {
            $this.addFill()
            $this.addRightBorder()
            $this.addNewLine()
            $this.addLeftBorder()
        }
    }

    hidden [void] processSeparator([FortahSeparator] $pSeparator) {
        $this.addFill()
        $this.addRightBorder()
        $this.addNewLine()
        $this.addMiddleBorder()
        $this.addNewLine()
        $this.addLeftBorder()        
    }

    hidden [void] processControl([FortahControl] $pControl) {
        $this.renderables.Add($pControl)
        $this.currentWidth += $pControl.calculateWidth()
    }

    hidden [void] addFooter() {
        $this.addFill()
        $this.addRightBorder()
        $this.addNewLine()
        $this.addBottomBorder()
    }

    hidden [void] addNewLine() {
        $this.renderables.Add([FortahNewLine]::new())
        $this.currentWidth = 0
    }

    hidden [void] addTopBorder() {
        $this.addHorizontalBorder([FortahCharacter]::topLeft, [FortahCharacter]::topRight)
    }

    hidden [void] addMiddleBorder() {
        $this.addHorizontalBorder([FortahCharacter]::leftJoin, [FortahCharacter]::rightJoin)
    }

    hidden [void] addBottomBorder() {
        $this.addHorizontalBorder([FortahCharacter]::bottomLeft, [FortahCharacter]::bottomRight)
    }    

    hidden [void] addHorizontalBorder([char] $pLeftChar, [char] $pRightChar) {
        $this.renderables.Add([FortahLabel]::new($pLeftChar + ([FortahCharacter]::horizontal.ToString() * ($this.outerWidth - 2)) + $pRightChar, $this.colour))
    }

    hidden [void] addLeftBorder() {
        $this.renderables.Add([FortahLabel]::new([FortahCharacter]::vertical + ' ', $this.colour))
    }

    hidden [void] addRightBorder() {
        $this.renderables.Add([FortahLabel]::new(' ' + [FortahCharacter]::vertical, $this.colour))
    }

    hidden [void] addFill() {
        if ($this.currentWidth -lt $this.width) {
            $this.renderables.Add([FortahLabel]::new(' ' * ($this.width - $this.currentWidth)))
        }
    }    
    
}

class FortahWindow : FortahContainer {
    
    hidden [string] $userInput = $null
    [string] getUserInput() { return $this.userInput }

    FortahWindow() : base() {        
    }

    FortahWindow([List[FortahControl]] $pControls) : base($pControls) {
    }

    [void] render() {
        [List[FortahControl]] $renderables = $this.toRenderables()
        $this.userInput = ''
        [FortahControl] $renderable = $null
        foreach ($renderable in $renderables) {
            $renderable.render()
            [string] $renderableUserInput = $renderable.getUserInput()
            if (($null -ne $renderableUserInput) -and ($renderableUserInput.Length -gt 0)) {
                $this.userInput = $renderableUserInput
            }
        }
    }
}

class FortahReader : FortahControl {
    
    hidden [string] $prompt = ''
    [string] getPrompt() { return $this.prompt }
    [void] setPrompt([string] $pValue) { $this.prompt = $pValue }

    hidden [string] $userInput = ''
    [string] getUserInput() { return $this.userInput }

    FortahReader([string] $pPrompt) : base([FortahColours]::commandColour) {
        $this.prompt = $pPrompt
    }

    FortahReader([string] $pName, [string] $pPrompt) : base($pName, [FortahColours]::commandColour) {
        $this.prompt = $pPrompt
    }

    FortahReader([string] $pPrompt, [FortahColour] $pColour) : base($pColour) {
        $this.prompt = $pPrompt
    }

    FortahReader([string] $pName, [string] $pPrompt, [FortahColour] $pColour) : base($pName, $pColour) {
        $this.prompt = $pPrompt
    }

    static [FortahReader] createDefaultReader() {
        return [FortahReader]::new('Please, press "Enter" to continue')
    }
    
    static [FortahReader] createCommandReader() {
        return [FortahReader]::new('Please, type your command and press "Enter"')
    }

    [List[FortahControl]] toRenderables() {
        return [List[FortahControl]]::new(@($this))
    }    
    
    [void] render() {
        $this.userInput = [FortahConsole]::readLine("$($this.prompt): ", $this.colour)
    }
}

class FortahGridColumn {

    hidden [string] $header = ''
    [string] getHeader() { return $this.header }
    [void] setHeader([string] $pValue) { $this.header = $pValue }
    
    hidden [int] $width = 0
    [int] getWidth() { return $this.width }
    [void] setWidth([int] $pValue) { $this.width = $pValue }

    hidden [FortahColour] $colour = [FortahColour]::null
    [FortahColour] getColour() { return $this.colour }
    [void] setColour([FortahColour] $pValue) { $this.colour = $pValue }

    hidden [ScriptBlock] $binding = $null
    [ScriptBlock] getBinding() { return $this.binding }
    [void] setBinding([ScriptBlock] $pValue) { $this.binding = $pValue }

    FortahGridColumn([string] $pHeader, [int] $pWidth) {
        $this.header = $pHeader
        $this.width = $pWidth
    }

    FortahGridColumn([string] $pHeader, [int] $pWidth, [FortahColour] $pColour) {
        $this.header = $pHeader
        $this.width = $pWidth
        $this.colour = $pColour
    }

    FortahGridColumn([string] $pHeader, [int] $pWidth, [ScriptBlock] $pBinding) {
        $this.header = $pHeader
        $this.width = $pWidth
        $this.binding = $pBinding
    }

    FortahGridColumn([string] $pHeader, [int] $pWidth, [FortahColour] $pColour, [ScriptBlock] $pBinding) {
        $this.header = $pHeader
        $this.width = $pWidth
        $this.colour = $pColour
        $this.binding = $pBinding
    }

    [Object] getValue([Object] $pData, [int] $pIndex) {
        [Object] $value = $null
        if ($null -ne $this.binding) {
            $value = $this.binding.Invoke($pData)
        } else {
            $value = $pData[$pIndex]
        }
        return $this.transformValue($value)
    }

    [Object] transformValue([Object] $pValue) {
        return $pValue
    }

    [FortahLabel] toHeaderLabel() {
        return $this.toLabel($this.header, [FortahColours]::headerColour)
    }

    [FortahLabel] toLabel([Object] $pValue) {
        return $this.toLabel($pValue, [FortahState]::normal)
    }
    
    [FortahLabel] toLabel([Object] $pValue, [FortahState] $pState) {
        [FortahColour] $labelColour = $this.colour
        if ($labelColour -eq [FortahColour]::null) {
            $labelColour = [FortahColours]::getStateColour($pState)
        }
        return $this.toLabel($pValue, $labelColour)
    }

    [FortahLabel] toLabel([Object] $pValue, [FortahColour] $pColour) {
        return [FortahLabel]::new($pValue, $this.width, $pColour)
    }       
}

class FortahGridCheckboxColumn : FortahGridColumn {
    
    FortahGridCheckboxColumn([string] $pHeader, [int] $pWidth) : base($pHeader, $pWidth) {
    }

    [Object] transformValue([Object] $pValue) {
        if ($pValue) {
            return [FortahCharacter]::on
        } else {
            return [FortahCharacter]::off
        }
    }
            
}

class FortahGrid : FortahFrame {

    hidden [string] $header = ''
    [string] getHeader() { return $this.header }
    [void] setHeader([string] $pValue) { $this.header = $pValue }

    hidden [List[FortahGridColumn]] $columns = [List[FortahGridColumn]]::new()
    [List[FortahGridColumn]] getColumns() { return $this.columns }
    [void] setColumns([List[FortahGridColumn]] $pValue) { $this.columns = $pValue }

    hidden [ScriptBlock] $stateBinding = $null
    [ScriptBlock] getStateBinding() { return $this.stateBinding }
    [void] setStateBinding([ScriptBlock] $pValue) { $this.stateBinding = $pValue }

    hidden [Object] $data = [Object]::new()
    [Object] getData() { return $this.data }
    [void] setData([Object] $pValue) { $this.data = $pValue }

    FortahGrid() : base() {        
    }

    FortahGrid([string] $pName) : base($pName) {
    }

    FortahGrid([List[FortahGridColumn]] $pColumns) : base() {
        $this.columns = $pColumns
    }

    FortahGrid([int] $pWidth, [List[FortahGridColumn]] $pColumns) : base() {
        $this.width = $pWidth
        $this.columns = $pColumns
    }

    FortahGrid([string] $pName, [int] $pWidth, [List[FortahGridColumn]] $pColumns) : base($pName) {
        $this.width = $pWidth
        $this.columns = $pColumns
    }

    FortahGrid([string] $pHeader, [List[FortahGridColumn]] $pColumns) : base() {
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    FortahGrid([string] $pName, [string] $pHeader, [List[FortahGridColumn]] $pColumns) : base($pName) {
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    FortahGrid([int] $pWidth, [string] $pHeader, [List[FortahGridColumn]] $pColumns) : base() {
        $this.width = $pWidth
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    FortahGrid([string] $pName, [int] $pWidth, [string] $pHeader, [List[FortahGridColumn]] $pColumns) : base($pName) {
        $this.width = $pWidth
        $this.header = $pHeader 
        $this.columns = $pColumns
    }

    [List[FortahControl]] toRenderables() {
        $this.updateWidth()
        $this.controls = [List[FortahControl]]::new()
        $this.addHeaderControls()
        $this.addColumnHeadersControls()
        $this.addDataControls()
        return ([FortahFrame]$this).toRenderables()
    }

    hidden [void] addHeaderControls() {
        if ($this.header.Length -gt 0) {
            $this.controls.Add([FortahLabel]::new($this.header), [FortahColours]::headerColour)
            $this.controls.Add([FortahSeparator]::new())
        }
    }

    hidden [void] addColumnHeadersControls() {
        if ($null -ne ($this.columns | Where-Object {$_.getHeader().Length -gt 0})) {
            [FortahShelf] $shelf = [FortahShelf]::new()
            [FortahGridColumn] $column = $null
            foreach ($column in $this.columns) {
                $shelf.addControl($column.toHeaderLabel())
            }
            $this.controls.Add($shelf)
            $this.controls.Add([FortahSeparator]::new())
        }        
    }

    hidden [void] addDataControls() {
        [FortahStack] $stack = [FortahStack]::new()
        [List[Object]] $dataRow = $null
        foreach ($dataRow in $this.data) {
            [FortahShelf] $shelf = [FortahShelf]::new()
            [FortahState] $state = $this.getRowState($dataRow)
            [int] $index = 0
            [FortahGridColumn] $column = $null
            foreach ($column in $this.columns) {
                [Object] $value = $column.getValue($dataRow, $index)
                $shelf.addControl($column.toLabel($value, $state))
                $index++
            }
            $stack.addControl($shelf)
        }
        $this.controls.Add($stack)
    }

    hidden [FortahState] getRowState([List[Object]] $pDataRow) {
        [FortahState] $state = [FortahState]::normal
        if ($null -ne $this.stateBinding) {
            $state = $this.stateBinding.Invoke($pDataRow)
        }
        return $state
    }

    hidden [void] updateWidth() {
        [int] $titleWidth = 0
        if ($this.title -ne '') {
            $titleWidth = $this.title.Length + 4
            if ($titleWidth -gt $this.width) {
                $this.width = $titleWidth
            }
        }
        [int] $columnsWidth = 2
        [FortahGridColumn] $column = $null
        foreach ($column in $this.columns) {
            $columnsWidth += $column.width + 2
        }
        if ($columnsWidth -gt $this.width) {
            $this.width = $columnsWidth
        }
    }     
     
}

class FortahPropertyGridGeometry {

    hidden [int] $nameWidth = 0
    [int] getNameWidth() { return $this.nameWidth }
    [void] setNameWidth([int] $pValue) { $this.nameWidth = $pValue }

    hidden [int] $valueWidth = 0
    [int] getValueWidth() { return $this.valueWidth }
    [void] setValueWidth([int] $pValue) { $this.valueWidth = $pValue }

    FortahPropertyGridGeometry() {
    }

    FortahPropertyGridGeometry([int] $pNameWidth) {
        $this.nameWidth = $pNameWidth
    }

    FortahPropertyGridGeometry([int] $pNameWidth, [int] $pValueWidth) {
        $this.nameWidth = $pNameWidth
        $this.valueWidth = $pValueWidth
    }

    static [List[int]] parse([List[int]] $pWidths) {
        [List[FortahPropertyGridGeometry]] $geometries = [List[FortahPropertyGridGeometry]]::new()
        [FortahPropertyGridGeometry] $geometry = $null
        for ([int] $index = 0; $index -lt $pWidths.Count; $index++) {
            if ($index % 2 -eq 0) {
                $geometry = [FortahPropertyGridGeometry]::new($pWidths[$index])
            } else {
                $geometry.setValueWidth($pWidths[$index])
                $geometries.Add($geometry)
            }
        }
        return $geometries
    }
    
}

class FortahPropertyGrid : FortahGrid {

    FortahPropertyGrid([List[int]] $pColumnWidths) : base() {
        $this.createColumns($pColumnWidths)

    }

    FortahPropertyGrid([string] $pHeader, [List[int]] $pColumnWidths) : base($pHeader) {
        $this.createColumns($pColumnWidths)
    }

    FortahPropertyGrid([string] $pName, [string] $pHeader, [List[int]] $pColumnWidths) : base($pName, $pHeader) {
        $this.createColumns($pColumnWidths)
    }

    hidden [void] createColumns([List[int]] $pColumnWidths) {
        [List[FortahPropertyGridGeometry]] $geometries = [FortahPropertyGridGeometry]::parse($pColumnWidths)
        [FortahPropertyGridGeometry] $geometry = $null
        foreach ($geometry in $geometries) {
            $this.columns.Add([FortahGridColumn]::new('', $geometry.getNameWidth(), [FortahColours]::textColour))
            $this.columns.Add([FortahGridColumn]::new('', $geometry.getValueWidth(), [FortahColours]::selectedColour))
        }
    }
    
}

class FortahSelectableGridItem {

    hidden [FortahCommand] $command = [FortahCommand]::new()
    [FortahCommand] getCommand() { return $this.command }
    [void] setCommand([FortahCommand] $pValue) { $this.command = $pValue }

    hidden [Object] $data = [Object]::new()
    [Object] getData() { return $this.data }
    [void] setData([Object] $pValue) { $this.data = $pValue }

    hidden [bool] $selected = $false
    [bool] isSelected() { return $this.selected }
    [void] setSelected([bool] $pValue) { $this.selected = $pValue }

    FortahSelectableGridItem([FortahCommand] $pCommand, [Object] $pData) {
        $this.setCommand($pCommand)
        $this.setData($pData)
    }

    FortahSelectableGridItem([FortahCommand] $pCommand, [Object] $pData, [bool] $pSelected) {
        $this.setCommand($pCommand)
        $this.setData($pData)
        $this.setSelected($pSelected)
    }    

}

class FortahSelectableGrid : FortahGrid {
    
    hidden [List[Object]] $items = [List[Object]]::new()
    [List[Object]] getItems() { return $this.items }
    [void] setItems([List[Object]] $pValue) { $this.items = $pValue }

    hidden [int] $commandIndex = 0
    [int] getCommandIndex() { return $this.commandIndex }
    [void] setCommandIndex([int] $pValue) { $this.commandIndex = $pValue }

    FortahSelectableGrid([int] $pWidth) : base($pWidth) {
        $this.FortahSelectableGridEx()
    }
    
    FortahSelectableGrid([string] $pHeader, [int] $pWidth) : base($pHeader, $pWidth) {
        $this.FortahSelectableGridEx()
    }

    FortahSelectableGrid([string] $pHeader, [int] $pWidth, [List[Object]] $pColumns) : base($pHeader, $pWidth) {
        $this.FortahSelectableGridEx()
        $this.addColumns($pColumns)
    }

    hidden [void] FortahSelectableGridEx() {
        $this.addColumns([List[FortahGridColumn]]::new(@(
            [FortahGridCheckboxColumn]::new('Sel', 3),
            [FortahGridColumn]::new('Com', 3, [FortahSelectableGrid]::createCommandColours())
        )))
    }

    [void] renderRow([Object] $pData) {
        $this.commandIndex++
        [string] $commandKey = $this.commandIndex.ToString()
        [FortahCommand] $command = [FortahCommand]::new('Select' + $commandKey, $commandKey, $commandKey)
        [FortahSelectableGridItem] $item = [FortahSelectableGridItem]::new($command, $pData)
        $this.items.Add($item)
        [List[object]] $dataToRender = $null
        if ($pData -is [List[object]]) {
            $dataToRender = $pData
        } else {
            $dataToRender = [List[object]]::new(@(pData))
        }
        $dataToRender.Insert(0, $item.isSelected())
        $dataToRender.Insert(1, $item.getCommand().getKey())
        ([FortahGrid]$this).renderRow($dataToRender)
    }
}

class FortahMenuItem {

    FortahMenuItem() {        
    }

    [List[FortahControl]] toControls([int] $pCommandKeyLength) {
        return [List[FortahControl]]::new()
    }
    
}

class FortahMenuSeparator : FortahMenuItem {
    
    FortahMenuSeparator() : base() {
    }

    [List[FortahControl]] toControls([int] $pCommandKeyLength) {
        return [List[FortahControl]]::new(@([FortahSeparator]::new()))
    }    
    
}

class FortahMenuCommand : FortahMenuItem {

    hidden [FortahCommand] $command = [FortahCommand]::new()
    [FortahCommand] getCommand() { return $this.command }
    [void] setCommand([FortahCommand] $pValue) { $this.command = $pValue }

    FortahMenuCommand([FortahCommand] $pCommand) : base() {
        $this.command = $pCommand
    }

    FortahMenuCommand([string] $pCommandCode, [string] $pCommandKey, [string] $pCommandText) {
        $this.command = [FortahCommand]::new($pCommandCode, $pCommandKey, $pCommandText)
    }        

    [List[FortahControl]] toControls([int] $pCommandKeyLength) {
        return [FortahControl]::new([List[FortahControl]]::new(@([FortahShelf]::new([List[FortahControl]]::new(@(
            [FortahLabel]::new($this.command.getKey(), $pCommandKeyLength, [FortahColours]::commandColour),
            [FortahLabel]::new($this.command.getText(), [FortahColours]::selectedColour)
        ))))))
    }
}

class FortahMenu : FortahFrame {
    
    hidden [string] $header = ''
    [string] getHeader() { return $this.header }
    [void] setHeader([string] $pValue) { $this.header = $pValue }

    hidden [List[FortahMenuItem]] $items = [List[FortahMenuItem]]::new()
    [List[FortahMenuItem]] getItems() { return $this.items }
    [void] setItems([List[FortahMenuItem]] $pValue) { $this.items = $pValue }

    FortahMenu([List[FortahMenuItem]] $pItems) : base() {
        $this.items = $pItems
    }

    FortahMenu([string] $pHeader, [List[FortahMenuItem]] $pItems) : base() {
        $this.header = $pHeader
        $this.items = $pItems
    }

    FortahMenu([int] $pWidth, [List[FortahMenuItem]] $pItems) : base($pWidth) {
        $this.items = $pItems
    }

    FortahMenu([int] $pWidth, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth) {
        $this.header = $pHeader
        $this.items = $pItems
    }

    [List[FortahControl]] toRenderables() {
        $this.controls = [List[FortahControl]]::new()
        $this.addHeaderControls()
        $this.addItemsControls()
        return ([FortahFrame]$this).toRenderables()
    }

    hidden [void] addHeaderControls() {
        if ($this.header.Length -gt 0) {
            $this.controls.Add([FortahLabel]::new($this.header, [FortahColours]::headerColour))
            $this.controls.Add([FortahSeparator]::new())
        }
    }

    hidden [void] addItemsControls() {
    }

    [FortahCommand] findCommand([string] $pKey) {
        [FortahCommand] $command = $null
        [FortahMenuItem] $item = ($this.items | Where-Object {(($_ -is [FortahMenuCommand]) -and ($_.getCommand().getKey() -eq $pKey))})
        if ($null -ne $item) {
            $command = $item.getCommand()
        }
        return $command
    }      
}

class FortahHorizontalMenu : FortahMenu {
    
    hidden [int] $space = 1
    [int] getSpace() { return $this.space }
    [void] setSpace([int] $pValue) { $this.space = $pValue }

    FortahHorizontalMenu([List[FortahMenuItem]] $pItems) : base($pItems) {
    }

    FortahHorizontalMenu([string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pHeader, $pItems) {
    }

    FortahHorizontalMenu([int] $pWidth, [List[FortahMenuItem]] $pItems) : base($pWidth, $pItems) {
    }

    FortahHorizontalMenu([int] $pWidth, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth, $pHeader, $pItems) {
    }

    FortahHorizontalMenu([int] $pWidth, [int] $pSpace, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth, $pHeader, $pItems) {
        $this.space = $pSpace
    }

    [void] addItemsControls() {
        [FortahShelf] $shelf = [FortahShelf]::new($this.space)
        [FortahMenuItem] $item = $null
        foreach ($item in $this.items) {
            $shelf.addControls($item.toControls(0))
        }
        $this.controls.Add($shelf)
    }    
}

class FortahVerticalMenu : FortahMenu {
    
    FortahVerticalMenu([List[FortahMenuItem]] $pItems) : base($pItems) {
    }

    FortahVerticalMenu([string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pHeader, $pItems) {
    }

    FortahVerticalMenu([int] $pWidth, [List[FortahMenuItem]] $pItems) : base($pWidth, $pItems) {
    }

    FortahVerticalMenu([int] $pWidth, [string] $pHeader, [List[FortahMenuItem]] $pItems) : base($pWidth, $pHeader, $pItems) {
    }

    [void] addItemsControls() {
        [int] $commandKeyLength = $this.calculateCommandKeyLength()
        [FortahStack] $stack = [FortahStack]::new()
        [FortahMenuItem] $item = $null
        foreach ($item in $this.items) {
            $stack.addControls($item.toControls($commandKeyLength))
        }
        $this.controls.Add($stack)
    }    

    hidden [int] calculateCommandKeyLength() {
        [int] $commandKeyLength = 0
        [List[FortahMenuCommand]] $commandsItems = [List[FortahMenuCommand]]::new(($this.items | Where-Object {$_ -is [FortahMenuCommand]}))
        if ($null -ne $commandsItems) {
            [FortahMenuCommand] $commandItem = $null
            foreach ($commandItem in $commandsItems) {
                [int] $commandItemKeyLength = $commandItem.getCommand().getKey().Length 
                if ($commandItemKeyLength -gt $commandKeyLength) {
                    $commandKeyLength = $commandItemKeyLength
                }
            }
        }
        return $commandKeyLength
    }
    
}

